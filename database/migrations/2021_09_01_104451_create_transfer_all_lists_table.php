<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransferAllListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_all_lists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pallet_management_id');
            $table->unsignedBigInteger('transfer_user_id');
            $table->foreign('pallet_management_id')->references('id')->on('pallet_management');
            $table->foreign('transfer_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_all_lists');
    }
}
