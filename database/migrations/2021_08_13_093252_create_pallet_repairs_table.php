<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePalletRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pallet_repairs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pallet_management_id');
            $table->unsignedBigInteger('repair_user_id');
            $table->foreign('pallet_management_id')->references('id')->on('pallet_management');
            $table->foreign('repair_user_id')->references('id')->on('users');
            $table->string('pallet_number_repairs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pallet_repairs');
    }
}
