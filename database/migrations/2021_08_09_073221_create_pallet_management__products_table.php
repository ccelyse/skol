<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePalletManagementProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pallet_management__products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pallet_management_id');
            $table->foreign('pallet_management_id')->references('id')->on('pallet_management');
            $table->string('pallet_product_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pallet_management__products');
    }
}
