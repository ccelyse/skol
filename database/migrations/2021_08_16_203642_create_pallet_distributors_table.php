<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePalletDistributorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pallet_distributors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pallet_management_id');
            $table->unsignedBigInteger('transfer_user_id');
            $table->unsignedBigInteger('distributor_id');
            $table->foreign('pallet_management_id')->references('id')->on('pallet_management');
            $table->foreign('transfer_user_id')->references('id')->on('users');
            $table->foreign('distributor_id')->references('id')->on('inventory_distributors');
            $table->string('distributor_return')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pallet_distributors');
    }
}
