<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BackendController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\LandingPage;
use Illuminate\Support\Facades\View;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', Homepage::class);
Route::get('/', [BackendController::class, 'login']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('redirects', [BackendController::class, 'index']);
    /*ADMIN ROUTES*/
    Route::prefix('Admin')->group(function () {
        Route::get('/Dashboard', \App\Http\Livewire\Backend\Admin\Dashboard::class);
        Route::get('/AdminUsers', \App\Http\Livewire\Backend\Admin\AdminUsers::class);
        Route::get('/InventoryLocation', \App\Http\Livewire\Backend\Admin\InvetoryLocation::class);
        Route::get('/InventoryProducts', \App\Http\Livewire\Backend\Admin\InventoryProducts::class);
        Route::get('/InventoryDistributors', \App\Http\Livewire\Backend\Admin\InventoryDistributors::class);
        Route::get('/InventoryTrucks', \App\Http\Livewire\Backend\Admin\InventoryTrucks::class);
        Route::get('/PalletManagement', \App\Http\Livewire\Backend\Admin\PalletManagement::class);
        Route::get('/UniversalSearch', \App\Http\Livewire\Backend\Admin\UniversalSearch::class);
        Route::get('/Reports', \App\Http\Livewire\Backend\Admin\Reports::class);
        Route::get('/PendingTransfers', \App\Http\Livewire\Backend\Admin\PendingTransfers::class);
//        Route::get('/ScanPallet', \App\Http\Livewire\Backend\Admin\ScanPallet::class);
        Route::get('/PrintPallet/{id}', [BackendController::class, 'PrintPallet']);
        Route::get('/ScanPallet', [BackendController::class, 'ScanPallet']);
        Route::get('/ScanPallet-Transfer/{id}', \App\Http\Livewire\Backend\Admin\ScanPallet::class);
        Route::get('/PalletTransfer/{id}', \App\Http\Livewire\Backend\Admin\Pallettransfer::class);
    });

    /*USER ROUTES*/

    Route::prefix('User')->group(function () {
        Route::get('/Dashboard', \App\Http\Livewire\Backend\User\Dashboard::class);
        Route::get('/UserAccount', \App\Http\Livewire\Backend\User\Account::class);
        Route::get('/ScanPallet', \App\Http\Livewire\Backend\Admin\ScanPallet::class);
        Route::get('/PrintPallet/{id}', [BackendController::class, 'PrintPallet']);
        Route::get('/PalletTransfer/{id}', \App\Http\Livewire\Backend\Admin\Pallettransfer::class);
        Route::get('/PrintPallet/{id}', [BackendController::class, 'PrintPallet']);
        Route::get('/ScanPallet', [BackendController::class, 'ScanPallet']);
        Route::get('/ScanPallet-Transfer/{id}', \App\Http\Livewire\Backend\Admin\ScanPallet::class);

    });
});

//Route::get('/register', [BackendController::class, 'login']);
