<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function () {
//    Route::post('/UpdatePaymentSpenn',['as'=>'UpdatePaymentSpenn','uses'=>'FrontendController@UpdatePaymentSpenn']);
    Route::post('/UpdatePaymentSpenn', [\App\Http\Controllers\FrontendController::class, 'UpdatePaymentSpenn']);
    Route::post('/UpdatePaymentSpennCompetition', [\App\Http\Controllers\FrontendController::class, 'UpdatePaymentSpennCompetition']);
    Route::post('/UpdateMomoPayment', [\App\Http\Controllers\FrontendController::class, 'UpdateMomoPayment']);
    Route::post('/UpdateCompetitionMomoPayment', [\App\Http\Controllers\FrontendController::class, 'UpdateCompetitionMomoPayment']);
    Route::post('/VoteNowCard', [\App\Http\Controllers\FrontendController::class, 'VoteNowCard']);
    Route::post('/VoteCompetitionNowCard', [\App\Http\Controllers\FrontendController::class, 'VoteCompetitionNowCard']);
    Route::any('/contentTitles', [\App\Http\Controllers\FrontendController::class, 'load_tiles']);
});
