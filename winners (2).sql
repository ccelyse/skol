-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 15, 2021 at 03:45 AM
-- Server version: 8.0.23
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mniv2_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `winners`
--

CREATE TABLE `winners` (
  `id` int NOT NULL,
  `artist_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `song_title` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `total_votes` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `period` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `winners`
--

INSERT INTO `winners` (`id`, `artist_name`, `song_title`, `cover`, `total_votes`, `period`, `created_at`, `updated_at`) VALUES
(1, 'Alex', 'mista', '', '3000', 'january - Appril 2021', '2021-04-14 22:26:30', '2021-04-14 21:06:13'),
(3, 'test', 'fffffj', 'SongImages/TgujEh1gCOqOP06KonriNS42QzH4KtvgeemhOTOd.png', '7980', 'jinew', '2021-04-14 21:43:05', '2021-04-14 22:01:12'),
(4, 'test123', 'azfasga', '', '525', 'sgdgdga', '2021-04-14 22:02:38', '2021-04-14 22:02:38'),
(5, 'yeeada', 'ddadas', 'Winners/A60vu2e97Shi2Sbm49LxylqX38Or93QiYEkLP6A7.png', '321', 'dd', '2021-04-14 22:07:00', '2021-04-14 22:11:25'),
(6, 'hjjdhh', 'ADAADAD', 'Winners/8aZLkTkwzF5safd55Jg6mkELCY5u6qcoGnQXcrDM.png', '1421', 'ADSDDD', '2021-04-14 22:37:49', '2021-04-14 22:38:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `winners`
--
ALTER TABLE `winners`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `winners`
--
ALTER TABLE `winners`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
