<html>
<head>
    <title>Html-Qrcode Demo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
    @media only screen and (max-width : 768px) {
        #qr-reader{
            width: 100% !important;
            position: relative;
            padding: 0px;
            border: 1px solid silver;
            height: 500px !important;
        }
        #qr-reader__dashboard_section_csr button{
            height: 100px !important;
            font-size: 17px !important;
            margin-top: 16px !important;
        }
        #check_transfer{
            font-size: 41px !important;
            margin-top: 20px !important;
        }
        #qr-reader__camera_selection{
            font-size: 16px !important;
            padding: 16px !important;
        }
    }

    btn:not(:disabled):not(.disabled) {
        cursor: pointer;
    }
    .btn {
        padding: 0.4375rem 1.25rem;
        text-shadow: none;
        font-size: 16px;
        color: #3b3f5c;
        font-weight: normal;
        white-space: normal;
        word-wrap: break-word;
        transition: .2s ease-out;
        touch-action: manipulation;
        cursor: pointer;
        background-color: #c0c4c7;
        box-shadow: 0px 5px 20px 0 rgb(0 0 0 / 10%);
        will-change: opacity, transform;
        transition: all 0.3s ease-out;
        -webkit-transition: all 0.3s ease-out;
        cursor: pointer !important;
        text-decoration: none;
    }
</style>
<body>
<div id="qr-reader" style="width:500px"></div>
<div id="qr-reader-results"></div>
@if(auth()->user()->role == "Admin")
    <a href="{{'/Admin/PendingTransfers'}}" style="float: left;" class="btn" id="check_transfer">Check all pending transfers</a>
@elseif(auth()->user()->role == "User")
    <a href="{{'/User/PendingTransfers'}}" style="float: left;" class="btn" id="check_transfer">Pending Transfers</a>
@endif
</body>
{{--<script src="/html5-qrcode.min.js"></script>--}}
<script src="https://unpkg.com/html5-qrcode@2.0.11/dist/html5-qrcode.min.js"></script>
<script>
    function docReady(fn) {
        // see if DOM is already available
        if (document.readyState === "complete"
            || document.readyState === "interactive") {
            // call on next available tick
            setTimeout(fn, 1);
        }else{
            document.addEventListener("DOMContentLoaded", fn);
        }
    }

    docReady(function () {
        var resultContainer = document.getElementById('qr-reader-results');
        var lastResult, countResults = 0;
        function onScanSuccess(decodedText, decodedResult) {
            if (decodedText !== lastResult) {
                ++countResults;
                lastResult = decodedText
                // var link = 'http://127.0.0.1:9010/Admin/ScanPallet-Transfer/' + lastResult;
                var link = 'https://www.skolpm.rw/Admin/ScanPallet-Transfer/' + lastResult;
                // Handle on success condition with the decoded message.
                alert(`Scan result ${decodedText}`, decodedResult);
                // alert(link);
                window.location = link;
            }
        }

        var html5QrcodeScanner = new Html5QrcodeScanner(
            "qr-reader", { fps: 10, qrbox: 250 });
        html5QrcodeScanner.render(onScanSuccess);
    });
</script>
</head>
</html>
