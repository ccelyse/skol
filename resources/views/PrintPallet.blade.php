<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Print Pallet</title>
    <style>
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        .container{
            /*border: 3px solid green;*/
            /*padding: 100px;*/
        }
        .certificate{
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            /*border: 3px solid #d4262d;*/
            padding: 10px;
            /*width: 90%;*/
        }
    </style>
</head>
<body class="" style="background: white; width: 100%; margin: 0;">
<div class="container">
    <div class="certificate">
        @php
            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
            echo '<img style="width:500px; height:300px" src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode_number, $generator::TYPE_CODE_128)) . '">';
        @endphp
        <h1 align="center">{{$barcode_number}}</h1>
    </div>
</div>
</body>
</html>
