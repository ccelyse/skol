@extends('Backend.Layouts.master')

@section('title', 'Bookstore| Registration')

@section('content')
    <style>
        .bg-primary, .badge-primary {
            color: var(--iq-white);
            background: #121c1e !important;
        }
        .btn-white {
            background-color: var(--iq-white);
            border-color: var(--iq-white);
            color: #9f321f !important;
        }
        .invalid-feedback {
            display: block;
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #dc3545;
        }
        .sign-in-from {
            width: 100% !important;
        }
        .sign-in-page{
            height: auto !important;
        }
    </style>
    <section class="sign-in-page">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-sm-12 align-self-center page-content rounded">
                    <div class="row m-0">
                        <div class="col-sm-12">
                            <div class="iq-card">
                                <div class="iq-card-header d-flex justify-content-between">
                                    <div class="iq-header-title">
                                        <h4 class="card-title text-center">Sign Up</h4>
                                    </div>
                                    <a href="{{'/'}}" class="btn iq-bg-primary">Go Back Home</a>
                                </div>
                                <div class="iq-card-body">
                                    <p>Enter your email address and password to access admin panel.</p>
                                    <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab-1" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-Reader-tab-fill" data-toggle="pill" href="#Reader" role="tab" aria-controls="pills-home" aria-selected="true">Reader</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-Author-tab-fill" data-toggle="pill" href="#Author" role="tab" aria-controls="pills-profile" aria-selected="false">Author</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-freelancer-tab-fill" data-toggle="pill" href="#Freelancer" role="tab" aria-controls="pills-profile" aria-selected="false">Freelancer</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent-1">
                                        <div class="tab-pane fade  active show" id="Reader" role="tabpanel" aria-labelledby="pills-Reader-tab-fill">
                                            <form action="{{route('register')}}" method="post" style="display: inline-flex; !important;" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-3">
                                                    <div class="iq-card">
                                                        <div class="iq-card-header d-flex justify-content-between">
                                                            <div class="iq-header-title">
                                                                <h4 class="card-title">User Profile</h4>
                                                            </div>
                                                        </div>
                                                        <div class="iq-card-body">
                                                            <div class="form-group">
                                                                <div class="add-img-user profile-img-edit">
                                                                    <img class="profile-pic img-fluid" src="images/user/11.png" alt="profile-pic">
                                                                    <div class="p-image">
                                                                        <a href="javascript:void();" class="upload-button btn iq-bg-primary">File Upload</a>
                                                                        <input class="file-upload" type="file" name="profile_picture" accept="image/*">
                                                                        @error('profile_picture')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="img-extension mt-3">
                                                                    <div class="d-inline-block align-items-center">
                                                                        <span>Only</span>
                                                                        <a href="javascript:void();">.jpg</a>
                                                                        <a href="javascript:void();">.png</a>
                                                                        <a href="javascript:void();">.jpeg</a>
                                                                        <span>allowed</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" hidden>
                                                                <label>User Role:</label>
                                                                <select class="form-control" name="role" id="selectuserrole">
                                                                    <option value="Reader">Reader</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label class="d-block">Gender:</label>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="customRadio6" name="gender"  value="male" class="custom-control-input" checked="">
                                                                    <label class="custom-control-label" for="customRadio6"> Male </label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="customRadio7" name="gender" value="Female" class="custom-control-input">
                                                                    <label class="custom-control-label" for="customRadio7"> Female </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="iq-card">
                                                        <div class="iq-card-header d-flex justify-content-between">
                                                            <div class="iq-header-title">
                                                                <h4 class="card-title">User Information</h4>
                                                            </div>
                                                        </div>
                                                        <div class="iq-card-body">
                                                            <div class="new-user-info">
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="fname">Name:</label>
                                                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Name">
                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label>Country:</label>
                                                                        <select class="form-control @error('country_id') is-invalid @enderror" name="country_id" id="selectcountry">
                                                                            <option>Select Country</option>
                                                                            @foreach($country as $list_country)
                                                                                <option value="{{$list_country->id}}">{{$list_country->country_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @error('country_id')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="mobno">Mobile Number:</label>
                                                                        <input type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" id="mobno" placeholder="Mobile Number">
                                                                        @error('phone_number')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="email">Email:</label>
                                                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email">
                                                                        @error('email')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <h5 class="mb-3">Security</h5>
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="pass">Password:</label>
                                                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="pass" placeholder="Password">
                                                                        @error('password')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="rpass">Repeat Password:</label>
                                                                        <input type="password" class="form-control" name="password_confirmation" id="rpass" placeholder="Repeat Password ">
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary">SignUp</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="Author" role="tabpanel" aria-labelledby="pills-Author-tab-fill">
                                            <form action="{{route('register')}}" method="post" style="display: inline-flex; !important;" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-3">
                                                    <div class="iq-card">
                                                        <div class="iq-card-header d-flex justify-content-between">
                                                            <div class="iq-header-title">
                                                                <h4 class="card-title">User Profile</h4>
                                                            </div>
                                                        </div>
                                                        <div class="iq-card-body">
                                                            <div class="form-group">
                                                                <div class="add-img-user profile-img-edit">
                                                                    <img class="profile-pic img-fluid" src="images/user/11.png" alt="profile-pic">
                                                                    <div class="p-image">
                                                                        <a href="javascript:void();" class="upload-button btn iq-bg-primary">File Upload</a>
                                                                        <input class="file-upload" type="file" name="profile_picture_author" accept="image/*">
                                                                            @error('profile_picture_author')
                                                                            <span class="invalid-feedback" role="alert">
                                                                              <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                </div>
                                                                <div class="img-extension mt-3">
                                                                    <div class="d-inline-block align-items-center">
                                                                        <span>Only</span>
                                                                        <a href="javascript:void();">.jpg</a>
                                                                        <a href="javascript:void();">.png</a>
                                                                        <a href="javascript:void();">.jpeg</a>
                                                                        <span>allowed</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>User Role:</label>
                                                                <select class="form-control" name="role" id="selectuserrole">
                                                                    <option value="Author">Author</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="furl">Facebook Url:</label>
                                                                <input type="text" class="form-control" name="author_facebook_url" id="author_facebook_url" placeholder="Facebook Url">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="turl">Twitter Url:</label>
                                                                <input type="text" class="form-control" name="author_twitter_url" id="author_twitter_url" placeholder="Twitter Url">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="instaurl">Instagram Url:</label>
                                                                <input type="text" class="form-control" name="author_instagram_url" id="author_instagram_url" placeholder="Instagram Url">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="lurl">Linkedin Url:</label>
                                                                <input type="text" class="form-control" name="author_linkedin_url" id="author_linkedin_url" placeholder="Linkedin Url">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="iq-card">
                                                        <div class="iq-card-header d-flex justify-content-between">
                                                            <div class="iq-header-title">
                                                                <h4 class="card-title">User Information</h4>
                                                            </div>
                                                        </div>
                                                        <div class="iq-card-body">
                                                            <div class="new-user-info">
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="fname">Names:</label>
                                                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Name">
                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="mobno">Mobile Number:</label>
                                                                        <input type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" id="mobno" placeholder="Mobile Number">
                                                                        @error('phone_number')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="email">Email:</label>
                                                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email">
                                                                        @error('email')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label>Country:</label>
                                                                        <select class="form-control @error('country_id') is-invalid @enderror" name="country_id" id="selectcountry">
                                                                            <option>Select Country</option>
                                                                            @foreach($country as $list_country)
                                                                                <option value="{{$list_country->id}}">{{$list_country->country_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @error('country_id')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="city">Town/City:</label>
                                                                        <input type="text" class="form-control" name="author_city" id="city" placeholder="Town/City">
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label for="add1">Street Address 1:</label>
                                                                        <input type="text" class="form-control" name="author_street_address" id="add1" placeholder="Street Address 1">
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="cname">Company Name:</label>
                                                                        <input type="text" class="form-control" name="author_company_name" id="author_company_name" placeholder="Company Name">
                                                                    </div>
                                                                    <div class="form-group col-sm-6">
                                                                        <label class="d-block">Gender:</label>
                                                                        <div class="custom-control custom-radio custom-control-inline">
                                                                            <input type="radio" id="customRadio6" name="gender" class="custom-control-input" checked="">
                                                                            <label class="custom-control-label" for="customRadio6"> Male </label>
                                                                        </div>
                                                                        <div class="custom-control custom-radio custom-control-inline">
                                                                            <input type="radio" id="customRadio7" name="gender" class="custom-control-input">
                                                                            <label class="custom-control-label" for="customRadio7"> Female </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <h5 class="mb-3">Security</h5>
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="pass">Password:</label>
                                                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password">
                                                                        @error('password')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="rpass">Repeat Password:</label>
                                                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Repeat Password ">
                                                                    </div>
                                                                </div>
                                                                <h5 class="mb-3">Author Information</h5>
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="pass">Author Name:</label>
                                                                        <input type="text" class="form-control" name="author_name" id="author_name" placeholder="">
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="pass">Author Website:</label>
                                                                        <input type="url" class="form-control" name="author_website" id="author_website" placeholder="">
                                                                    </div>
                                                                    <div class="form-group col-md-12">
                                                                        <label for="pass">Author Description:</label>
                                                                        <textarea class="form-control" name="author_description" rows="5"></textarea>
                                                                    </div>
                                                                </div>
                                                                <h5 class="mb-3">Payment details</h5>
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="pass">PayPal Merchant Id:</label>
                                                                        <input type="text" class="form-control" name="paypal_merchant_id" id="pass" placeholder="">
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary">SignUp</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="Freelancer" role="tabpanel" aria-labelledby="pills-freelancer-tab-fill">
                                            <form action="{{route('register')}}" method="post" style="display: inline-flex; !important;" enctype="multipart/form-data">
                                                @csrf
                                                <div class="col-lg-3">
                                                    <div class="iq-card">
                                                        <div class="iq-card-header d-flex justify-content-between">
                                                            <div class="iq-header-title">
                                                                <h4 class="card-title">User Profile</h4>
                                                            </div>
                                                        </div>
                                                        <div class="iq-card-body">
                                                            <div class="form-group">
                                                                <div class="add-img-user profile-img-edit">
                                                                    <img class="profile-pic img-fluid" src="images/user/11.png" alt="profile-pic">
                                                                    <div class="p-image">
                                                                        <a href="javascript:void();" class="upload-button btn iq-bg-primary">File Upload</a>
                                                                        <input class="file-upload" type="file" name="profile_picture_freelancer" accept="image/*">
                                                                        @error('profile_picture')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="img-extension mt-3">
                                                                    <div class="d-inline-block align-items-center">
                                                                        <span>Only</span>
                                                                        <a href="javascript:void();">.jpg</a>
                                                                        <a href="javascript:void();">.png</a>
                                                                        <a href="javascript:void();">.jpeg</a>
                                                                        <span>allowed</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>User Role:</label>
                                                                <select class="form-control" name="role" id="selectuserrole">
                                                                    <option value="Freelancer">Freelancer</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="furl">Facebook Url:</label>
                                                                <input type="text" class="form-control" name="freelancer_facebook_url" id="freelancer_facebook_url" placeholder="Facebook Url">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="turl">Twitter Url:</label>
                                                                <input type="text" class="form-control" name="freelancer_twitter_url" id="freelancer_twitter_url" placeholder="Twitter Url">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="instaurl">Instagram Url:</label>
                                                                <input type="text" class="form-control" name="freelancer_instagram_url" id="freelancer_instagram_url" placeholder="Instagram Url">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="lurl">Linkedin Url:</label>
                                                                <input type="text" class="form-control" name="freelancer_linkedin_url" id="freelancer_linkedin_url" placeholder="Linkedin Url">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="iq-card">
                                                        <div class="iq-card-header d-flex justify-content-between">
                                                            <div class="iq-header-title">
                                                                <h4 class="card-title">User Information</h4>
                                                            </div>
                                                        </div>
                                                        <div class="iq-card-body">
                                                            <div class="new-user-info">
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="fname">Names:</label>
                                                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Name">
                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="mobno">Mobile Number:</label>
                                                                        <input type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" id="mobno" placeholder="Mobile Number">
                                                                        @error('phone_number')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="email">Email:</label>
                                                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email">
                                                                        @error('email')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label>Country:</label>
                                                                        <select class="form-control @error('country_id') is-invalid @enderror" name="country_id" id="selectcountry">
                                                                            <option>Select Country</option>
                                                                            @foreach($country as $list_country)
                                                                                <option value="{{$list_country->id}}">{{$list_country->country_name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @error('country_id')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="city">Town/City:</label>
                                                                        <input type="text" class="form-control" name="freelancer_city" id="freelancer_city" placeholder="Town/City">
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label for="add1">Street Address 1:</label>
                                                                        <input type="text" class="form-control" name="freelancer_street_address" id="freelancer_street_address" placeholder="Street Address 1">
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="cname">Company Name:</label>
                                                                        <input type="text" class="form-control" name="freelancer_company_name" id="freelancer_company_name" placeholder="Company Name">
                                                                    </div>
                                                                    <div class="form-group col-sm-6">
                                                                        <label class="d-block">Gender:</label>
                                                                        <div class="custom-control custom-radio custom-control-inline">
                                                                            <input type="radio" id="customRadio6" name="gender" class="custom-control-input" checked="">
                                                                            <label class="custom-control-label" for="customRadio6"> Male </label>
                                                                        </div>
                                                                        <div class="custom-control custom-radio custom-control-inline">
                                                                            <input type="radio" id="customRadio7" name="gender" class="custom-control-input">
                                                                            <label class="custom-control-label" for="customRadio7"> Female </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <h5 class="mb-3">Freelancer Information</h5>
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="pass">Freelancer Website:</label>
                                                                        <input type="url" class="form-control" name="freelancer_website" id="freelancer_website" placeholder="">
                                                                    </div>
                                                                    <div class="form-group col-md-12">
                                                                        <label for="pass">Freelancer Description:</label>
                                                                        <textarea class="form-control" name="freelancer_description" rows="5"></textarea>
                                                                    </div>
                                                                </div>
                                                                <h5 class="mb-3">Security</h5>
                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <label for="pass">Password:</label>
                                                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password">
                                                                        @error('password')
                                                                        <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="rpass">Repeat Password:</label>
                                                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Repeat Password ">
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary">Add New User</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
