<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>@yield('page-title') - MNI</title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="MNI,music,song,votes,music charts" />
    <meta name="keywords" content="MNI,music,song,music charts" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--Template style -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/fonts.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/flaticon.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/font-awesome.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/nice-select.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/swiper.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/magnific-popup.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/responsive.css')}}" />
{{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">--}}
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="{{asset('frontend/assets/img/logo_icon.png')}}" />
    <style>
        body{
            overflow-y: scroll;
            overflow-x: hidden;
        }
        /*html {*/
        /*    scroll-behavior: smooth !important;*/
        /*}*/
        .mainmenu ul li a {
            padding: 23px 15px !important;
        }
        /*!
 * Load Awesome v1.1.0 (http://github.danielcardoso.net/load-awesome/)
 * Copyright 2015 Daniel Cardoso <@DanielCardoso>
 * Licensed under MIT
 */
        .la-line-scale-pulse-out,
        .la-line-scale-pulse-out > div {
            position: relative;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .la-line-scale-pulse-out {
            display: inline-block;
            font-size: 0;
            color: #fff;
        }
        .la-line-scale-pulse-out.la-dark {
            color: #333;
        }
        .la-line-scale-pulse-out > div {
            display: inline-block;
            float: none;
            background-color: currentColor;
            border: 0 solid currentColor;
        }
        .la-line-scale-pulse-out {
            width: 40px;
            height: 32px;
        }
        .la-line-scale-pulse-out > div {
            width: 4px;
            height: 32px;
            margin: 2px;
            margin-top: 0;
            margin-bottom: 0;
            border-radius: 0;
            -webkit-animation: line-scale-pulse-out .9s infinite cubic-bezier(.85, .25, .37, .85);
            -moz-animation: line-scale-pulse-out .9s infinite cubic-bezier(.85, .25, .37, .85);
            -o-animation: line-scale-pulse-out .9s infinite cubic-bezier(.85, .25, .37, .85);
            animation: line-scale-pulse-out .9s infinite cubic-bezier(.85, .25, .37, .85);
        }
        .la-line-scale-pulse-out > div:nth-child(3) {
            -webkit-animation-delay: -.9s;
            -moz-animation-delay: -.9s;
            -o-animation-delay: -.9s;
            animation-delay: -.9s;
        }
        .la-line-scale-pulse-out > div:nth-child(2),
        .la-line-scale-pulse-out > div:nth-child(4) {
            -webkit-animation-delay: -.7s;
            -moz-animation-delay: -.7s;
            -o-animation-delay: -.7s;
            animation-delay: -.7s;
        }
        .la-line-scale-pulse-out > div:nth-child(1),
        .la-line-scale-pulse-out > div:nth-child(5) {
            -webkit-animation-delay: -.5s;
            -moz-animation-delay: -.5s;
            -o-animation-delay: -.5s;
            animation-delay: -.5s;
        }
        .la-line-scale-pulse-out.la-sm {
            width: 20px;
            height: 16px;
        }
        .la-line-scale-pulse-out.la-sm > div {
            width: 2px;
            height: 16px;
            margin: 1px;
            margin-top: 0;
            margin-bottom: 0;
        }
        .la-line-scale-pulse-out.la-2x {
            width: 80px;
            height: 64px;
        }
        .la-line-scale-pulse-out.la-2x > div {
            width: 8px;
            height: 64px;
            margin: 4px;
            margin-top: 0;
            margin-bottom: 0;
        }
        .la-line-scale-pulse-out.la-3x {
            width: 120px;
            height: 96px;
        }
        .la-line-scale-pulse-out.la-3x > div {
            width: 12px;
            height: 96px;
            margin: 6px;
            margin-top: 0;
            margin-bottom: 0;
        }
        /*
         * Animation
         */
        @-webkit-keyframes line-scale-pulse-out {
            0% {
                -webkit-transform: scaley(1);
                transform: scaley(1);
            }
            50% {
                -webkit-transform: scaley(.3);
                transform: scaley(.3);
            }
            100% {
                -webkit-transform: scaley(1);
                transform: scaley(1);
            }
        }
        @-moz-keyframes line-scale-pulse-out {
            0% {
                -moz-transform: scaley(1);
                transform: scaley(1);
            }
            50% {
                -moz-transform: scaley(.3);
                transform: scaley(.3);
            }
            100% {
                -moz-transform: scaley(1);
                transform: scaley(1);
            }
        }
        @-o-keyframes line-scale-pulse-out {
            0% {
                -o-transform: scaley(1);
                transform: scaley(1);
            }
            50% {
                -o-transform: scaley(.3);
                transform: scaley(.3);
            }
            100% {
                -o-transform: scaley(1);
                transform: scaley(1);
            }
        }
        @keyframes line-scale-pulse-out {
            0% {
                -webkit-transform: scaley(1);
                -moz-transform: scaley(1);
                -o-transform: scaley(1);
                transform: scaley(1);
            }
            50% {
                -webkit-transform: scaley(.3);
                -moz-transform: scaley(.3);
                -o-transform: scaley(.3);
                transform: scaley(.3);
            }
            100% {
                -webkit-transform: scaley(1);
                -moz-transform: scaley(1);
                -o-transform: scaley(1);
                transform: scaley(1);
            }
        }
    </style>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156034751-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-156034751-1');
    </script>
    @livewireStyles
    @stack('styles')
</head>
<body style="width: 100%;">

@livewire('frontend.top-nav')

{{$slot}}

<script src="{{asset('frontend/assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/modernizr.js')}}"></script>
<script src="{{asset('frontend/assets/js/plugin.js')}}"></script>
{{--<script src="{{asset('frontend/assets/js/jquery.nice-select.min.js')}}"></script>--}}
<script src="{{asset('frontend/assets/js/jquery.inview.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/jquery.magnific-popup.js')}}"></script>
<script src="{{asset('frontend/assets/js/swiper.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/comboTreePlugin.js')}}"></script>
<script src="{{asset('frontend/assets/js/mp3/jquery.jplayer.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/mp3/jplayer.playlist.js')}}"></script>
<script src="{{asset('frontend/assets/js/owl.carousel.js')}}"></script>
<script src="{{asset('frontend/assets/js/mp3/player.js')}}"></script>
<script src="{{asset('frontend/assets/js/custom.js')}}"></script>
<script>

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url: "../api/v1/user/contentTitles",
        dataType: 'json',
        success: function(response) {
            JSON.stringify(response);
            console.log(response);
            //
            jQuery('.slider_big_title').html(response.Keywords.slider_big_title);
            jQuery('.chart_tile').html(response.Keywords.chart_tile);
             jQuery('.previous_winner').html(response.Keywords.previous_winner);
            jQuery('.mni_moto').html(response.Keywords.mni_moto);
            jQuery('.voting_page_msg').html(response.Keywords.voting_page_msg);
            jQuery('.Count_Down_Title').html(response.Keywords.Count_Down_Title);
            jQuery('.Monthly_Chart_Title').html(response.Keywords.Monthly_Chart_Title);
        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
    });

    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
@livewireScripts
@stack('scripts')
</body>
</html>
