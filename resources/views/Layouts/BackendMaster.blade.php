<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('backend/assets/img/logo_icon.png')}}"/>
    <link href="{{ asset('backend/assets/css/loader.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('backend/assets/js/loader.js')}}"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{ asset('backend/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/components/tabs-accordian/custom-tabs.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{ asset('backend/plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/components/custom-carousel.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/css/forms/switches.css')}}">
{{--    <link href="{{ asset('backend/assets/css/dashboard/dash_2.css')}}" rel="stylesheet" type="text/css" />--}}
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/dropify/dropify.min.css')}}">
    <link href="{{ asset('backend/assets/css/users/account-setting.css" rel="stylesheet" type="text/css')}}" />
    <!--  END CUSTOM STYLE FILE  -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/table/datatable/dt-global_style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/css/elements/alert.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/bootstrap-select/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/editors/quill/quill.snow.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/assets/css/components/custom-modal.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/table/datatable/custom_dt_html5.css')}}">
    <link href="https://designreset.com/cork/ltr/demo3/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0420d5e0/dist/quagga.min.js"></script>

{{--    <link rel="stylesheet" href="{{ asset('backend/plugins/file-upload/file-upload-with-preview.min.css')}}">--}}
    <!-- END PAGE LEVEL STYLES -->
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <style>
        canvas.drawing, canvas.drawingBuffer {
            position: absolute;
            left: 0;
            top: 0;
        }
        #sidebar {
            height: 100vh!important;
            padding: 10px 0 10px 10px;
            backface-visibility: hidden;
            -webkit-backface-visibility: hidden;
            -webkit-transform: translate3d(0, 0, 0);
            background: #060818;
            overflow: auto !important;
        }
        .table > thead {
            border-top: 1px solid #191e3a;
            border-bottom: 1px solid #191e3a;
        }
        table.dataTable thead .sorting:before, table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:before, table.dataTable thead .sorting_desc_disabled:after {
            opacity: 1;
            color: #d3d3d3;
        }
        table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:after {
            color: #0e1726;
        }
        table.dataTable {
            border-collapse: separate;
            border-spacing: 0 5px;
            margin-top: 8px!important;
            margin-bottom: 50px!important;
            border-collapse: collapse!important;
            background: transparent;
        }
        .table > tbody:before {
            line-height: 1em;
            content: "_";
            color: transparent;
            display: block;
        }
        .table > tbody tr {
            border-radius: 4px;
            -webkit-transition: all 0.1s ease;
            transition: all 0.1s ease;
            border-bottom: 1px solid #191e3a;
            background: transparent!important;
        }
        .table > thead > tr > th {
            color: #009688;
            font-weight: 700;
            font-size: 13px;
            border: none;
            letter-spacing: 1px;
            text-transform: uppercase;
        }
        .table > tbody > tr > td {
            border: none;
            color: #888ea8;
            font-size: 13px;
            letter-spacing: 1px;
        }
        .table-striped tbody tr:nth-of-type(odd) {
            background-color: transparent!important;
        }
        .table > tfoot > tr > th {
            border: none;
        }
        .table-hover:not(.table-dark) tbody tr:hover {
            background-color: transparent!important;
            -webkit-transform: translateY(-1px) scale(1.01);
            transform: translateY(-1px) scale(1.01);
        }
        .table-hover.non-hover:not(.table-dark) tbody tr:hover {
            -webkit-transform: none;
            transform: none;
        }
        .dataTables_wrapper .dataTables_info {
            padding-top: 0.85em;
            white-space: normal;
            color: #009688;
            font-weight: 600;
            border: 1px solid #191e3a;
            display: inline-block;
            padding: 10px 16px;
            border-radius: 6px;
            font-size: 13px;
        }
        .dataTables_wrapper .dataTables_filter label {
            position: relative;
        }
        .dataTables_wrapper .dataTables_filter svg {
            position: absolute;
            top: 15px;
            right: 14px;
            width: 20px;
            color: #d3d3d3;
        }
        .dataTables_wrapper .form-control {
            background: #fff;
            border: none;
            margin-top: 5px;
            -webkit-box-shadow: 2px 5px 17px 0 rgba(31,45,61,.1);
            box-shadow: 2px 5px 17px 0 rgba(31,45,61,.1);
            border-radius: 6px;
            padding: 8px 30px 8px 14px;
            border: 1px solid #1b2e4b;
            color: #009688;
            font-size: 15px;
            background: #1b2e4b;
        }
        .dataTables_wrapper .dataTables_filter input {
            width: 150px;
        }
        .dataTables_wrapper .dataTables_length select.form-control {
            padding: 8px 17px 8px 14px;
            -moz-appearance:none; /* Firefox */
            -webkit-appearance:none; /* Safari and Chrome */
            appearance:none;
            background: #1b2e4b url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' width='20' height='20' viewBox='0 0 24 24' fill='none' stroke='%23bfc9d4' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-chevron-down'%3e%3cpolyline points='6 9 12 15 18 9'%3e%3c/polyline%3e%3c/svg%3e") 49px 12px no-repeat;
            border: 1px solid #1b2e4b;
            color: #009688;
            border-radius: 6px;
        }
        .dataTables_wrapper .dataTables_paginate {
            margin: 0;
            white-space: nowrap;
            text-align: right;
            display: inline-block;
        }
        .page-link {
            margin-right: 5px;
            border-radius: 50%;
            padding: 8px 12px;
            background: rgba(0, 23, 55, 0.08);
            border: none;
            color: #888ea8;
        }
        .page-link:focus {
            box-shadow: none;
        }
        .dataTables_wrapper .dataTables_paginate ul.pagination {
            margin: 3px 0;
        }
        .page-item.disabled .page-link {
            background: #191e3a;
        }
        .page-item .page-link:hover {
            background: #191e3a;
            color: #bfc9d4;
        }
        .page-item.disabled .page-link svg {
            color: #acb0c3;
        }
        .page-item:first-child .page-link {
            border-radius: 50%;
            padding: 5px 9px;
        }
        .page-item:first-child .page-link svg {
            width: 17px;
        }
        .page-item:last-child .page-link {
            border-radius: 50%;
            padding: 5px 9px;
        }
        .page-item:last-child .page-link svg {
            width: 17px;
        }
        .page-item.active .page-link {
            background-color: #009688;
        }
        #alter_pagination_next a, #alter_pagination_previous a {
            border-radius: 50%;
            padding: 5px 9px;
        }
        #alter_pagination_next a svg, #alter_pagination_previous a svg {
            width: 17px;
        }
        .table-cancel {
            color: #888ea8;
            margin-right: 6px;
            vertical-align: middle;
            fill: rgba(0, 23, 55, 0.08);
            cursor: pointer;
        }
        .table-hover:not(.table-dark) tbody tr:hover .table-cancel {
            color: #e7515a;
        }
        @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
            /* IE10+ CSS styles go here */
            .dataTables_wrapper .dataTables_length select.form-control {
                background: transparent;
                padding: 8px 10px 8px 14px;
            }
        }
        @media (max-width: 767px) {
            .dataTables_wrapper .dataTables_info {
                margin-bottom: 40px;
            }
        }
        /*
 * This combined file was created by the DataTables downloader builder:
 *   https://datatables.net/download
 *
 * To rebuild or modify this file with the latest versions of the included
 * software please visit:
 *   https://datatables.net/download/#bs4/dt-1.10.16
 *
 * Included libraries:
 *   DataTables 1.10.16
 */

        table.dataTable {
            clear: both;
            width: 100%!important;
            margin-top: 6px !important;
            margin-bottom: 6px !important;
            max-width: none !important;
            border-collapse: separate !important;
            border: none;
        }
        table.dataTable td,
        table.dataTable th {
            -webkit-box-sizing: content-box;
            box-sizing: content-box;
            padding: 9px 7px;
        }
        table.dataTable td.dataTables_empty,
        table.dataTable th.dataTables_empty {
            text-align: center;
        }
        table.dataTable.nowrap th,
        table.dataTable.nowrap td {
            white-space: nowrap;
        }
        .dataTables_wrapper .dataTables_length label {
            font-weight: normal;
            text-align: left;
            white-space: nowrap;
            color: #888ea8;
            font-weight: 600;
        }
        .dataTables_wrapper .dataTables_length select {
            width: 100px;
            display: inline-block !important;
        }
        .dataTables_wrapper .dataTables_filter {
            text-align: right;
        }
        .dataTables_wrapper .dataTables_filter label {
            font-weight: normal;
            white-space: nowrap;
            text-align: left;
            color: #888ea8;
            font-weight: 600;
        }
        .dataTables_wrapper .dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
        }
        table.dataTable .form-control {
            color: #888ea8;
            font-size: 0.875rem;
        }
        table.dataTable .form-control::-webkit-input-placeholder {
            color: #888ea8;
            font-size: 0.875rem;
        }
        table.dataTable .form-control::-ms-input-placeholder {
            color: #888ea8;
            font-size: 0.875rem;
        }
        table.dataTable .form-control::-moz-placeholder {
            color: #888ea8;
            font-size: 0.875rem;
        }

        .dataTables_wrapper .dataTables_info {
            padding-top: 0.85em;
            white-space: nowrap;
        }
        .dataTables_wrapper .dataTables_paginate {
            margin: 0;
            white-space: nowrap;
            text-align: right;
        }
        .dataTables_wrapper .dataTables_paginate ul.pagination {
            margin: 2px 0;
            white-space: nowrap;
            justify-content: flex-end;
        }
        .dataTables_wrapper .dataTables_processing {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 200px;
            margin-left: -100px;
            margin-top: -26px;
            text-align: center;
            padding: 1em 0;
        }
        table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting,
        table.dataTable thead > tr > td.sorting_asc,
        table.dataTable thead > tr > td.sorting_desc,
        table.dataTable thead > tr > td.sorting {
            padding-right: 30px;
        }
        table.dataTable thead > tr > th:active,
        table.dataTable thead > tr > td:active {
            outline: none;
        }
        table.dataTable thead .sorting,
        table.dataTable thead .sorting_asc,
        table.dataTable thead .sorting_desc,
        table.dataTable thead .sorting_asc_disabled,
        table.dataTable thead .sorting_desc_disabled {
            cursor: pointer;
            position: relative;
        }
        table.dataTable thead .sorting:before, table.dataTable thead .sorting:after,
        table.dataTable thead .sorting_asc:before,
        table.dataTable thead .sorting_asc:after,
        table.dataTable thead .sorting_desc:before,
        table.dataTable thead .sorting_desc:after,
        table.dataTable thead .sorting_asc_disabled:before,
        table.dataTable thead .sorting_asc_disabled:after,
        table.dataTable thead .sorting_desc_disabled:before,
        table.dataTable thead .sorting_desc_disabled:after {
            position: absolute;
            bottom: 0.9em;
            display: block;
            opacity: 0.4;
        }
        table.dataTable thead .sorting:before,
        table.dataTable thead .sorting_asc:before,
        table.dataTable thead .sorting_desc:before,
        table.dataTable thead .sorting_asc_disabled:before,
        table.dataTable thead .sorting_desc_disabled:before {
            right: 1em;
            content: "\2191";
        }
        table.dataTable thead .sorting:after,
        table.dataTable thead .sorting_asc:after,
        table.dataTable thead .sorting_desc:after,
        table.dataTable thead .sorting_asc_disabled:after,
        table.dataTable thead .sorting_desc_disabled:after {
            right: 0.5em;
            content: "\2193";
        }
        table.dataTable thead .sorting_asc:before,
        table.dataTable thead .sorting_desc:after {
            opacity: 1;
        }
        table.dataTable thead .sorting_asc_disabled:before,
        table.dataTable thead .sorting_desc_disabled:after {
            opacity: 0;
        }

        .dataTables_scrollHead table.dataTable {
            margin-bottom: 0 !important;
        }

        .dataTables_scrollBody table {
            border-top: none;
            margin-top: 0 !important;
            margin-bottom: 0 !important;
        }
        .dataTables_scrollBody table thead .sorting:after,
        .dataTables_scrollBody table thead .sorting_asc:after,
        .dataTables_scrollBody table thead .sorting_desc:after {
            display: none;
        }
        .dataTables_scrollBody table tbody tr:first-child th,
        .dataTables_scrollBody table tbody tr:first-child td {
            border-top: none;
        }

        .dataTables_scrollFoot > .dataTables_scrollFootInner {
            box-sizing: content-box;
        }
        .dataTables_scrollFoot > .dataTables_scrollFootInner > table {
            margin-top: 0 !important;
            border-top: none;
        }

        @media screen and (max-width: 767px) {
            .dataTables_wrapper .dataTables_length,
            .dataTables_wrapper .dataTables_filter,
            .dataTables_wrapper .dataTables_info,
            .dataTables_wrapper .dataTables_paginate {
                text-align: center;
            }
        }
        table.dataTable.table-sm > thead > tr > th {
            padding-right: 20px;
        }
        table.dataTable.table-sm .sorting:before,
        table.dataTable.table-sm .sorting_asc:before,
        table.dataTable.table-sm .sorting_desc:before {
            top: 5px;
            right: 0.85em;
        }
        table.dataTable.table-sm .sorting:after,
        table.dataTable.table-sm .sorting_asc:after,
        table.dataTable.table-sm .sorting_desc:after {
            top: 5px;
        }
        table.table-bordered.dataTable th,
        table.table-bordered.dataTable td {
            border-left-width: 0;
        }
        table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable th:last-child,
        table.table-bordered.dataTable td:last-child,
        table.table-bordered.dataTable td:last-child {
            border-right-width: 0;
        }
        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
        }
        .dataTables_scrollHead table.table-bordered {
            border-bottom-width: 0;
        }
        .table-responsive > .dataTables_wrapper > .row {
            margin: 0;
        }
        .table-responsive > .dataTables_wrapper > .row > div[class^="col-"]:first-child {
            padding-left: 0;
        }
        .table-responsive > .dataTables_wrapper > .row > div[class^="col-"]:last-child {
            padding-right: 0;
        }
        /*
    </style>
    @livewireStyles
    @stack('styles')
</head>
<body>
<!-- BEGIN LOADER -->
<div id="load_screen">
    <div class="loader">
        <div class="loader-content">
            <div class="spinner-grow align-self-center"></div>
        </div>
    </div>
</div>

<livewire:backend.header />
{{$slot}}

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('backend/assets/js/libs/jquery-3.1.1.min.js')}}"></script>
<script src="{{ asset('backend/bootstrap/js/popper.min.js')}}"></script>
<script src="{{ asset('backend/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('backend/assets/js/app.js')}}"></script>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script src="{{ asset('backend/assets/js/custom.js')}}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="{{ asset('backend/plugins/apex/apexcharts.min.js')}}"></script>
{{--<script src="{{ asset('backend/assets/js/dashboard/dash_1.js')}}"></script>--}}
<script src="{{ asset('backend/plugins/blockui/jquery.blockUI.min.js')}}"></script>

<script src="{{ asset('backend/plugins/highlight/highlight.pack.js')}}"></script>
<!-- END GLOBAL MANDATORY STYLES -->
<script src="{{ asset('backend/assets/js/scrollspyNav.js')}}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="{{ asset('backend/plugins/dropify/dropify.min.js')}}"></script>
<script src="{{ asset('backend/plugins/blockui/jquery.blockUI.min.js')}}"></script>
<!-- <script src="{{ asset('backend/plugins/tagInput/tags-input.js')}}"></script> -->
<script src="{{ asset('backend/assets/js/users/account-settings.js')}}"></script>
<!--  END CUSTOM SCRIPTS FILE  -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('backend/plugins/table/datatable/datatables.js')}}"></script>
<script src="{{ asset('backend/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{ asset('backend/plugins/editors/markdown/simplemde.min.js')}}"></script>
<script src="{{ asset('backend/plugins/editors/markdown/custom-markdown.js')}}"></script>
<script src="{{ asset('backend/plugins/table/datatable/button-ext/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('backend/plugins/table/datatable/button-ext/jszip.min.js')}}"></script>
<script src="{{ asset('backend/plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
<script src="{{ asset('backend/plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
{{--<script>--}}

{{--    $('#zero-config').DataTable({--}}
{{--        "oLanguage": {--}}
{{--            "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },--}}
{{--            "sInfo": "Showing page _PAGE_ of _PAGES_",--}}
{{--            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',--}}
{{--            "sSearchPlaceholder": "Search...",--}}
{{--            "sLengthMenu": "Results :  _MENU_",--}}
{{--        },--}}
{{--        "stripeClasses": [],--}}
{{--        // "lengthMenu": [7, 10, 20, 50],--}}
{{--        // "pageLength": 7,--}}
{{--    });--}}
{{--    $('#html5-extension').DataTable( {--}}
{{--        "dom": "<'dt--top-section'<'row'<'col-sm-12 col-md-6 d-flex justify-content-md-start justify-content-center'B><'col-sm-12 col-md-6 d-flex justify-content-md-end justify-content-center mt-md-0 mt-3'f>>>" +--}}
{{--            "<'table-responsive'tr>" +--}}
{{--            "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",--}}
{{--        buttons: {--}}
{{--            buttons: [--}}
{{--                { extend: 'copy', className: 'btn btn-sm' },--}}
{{--                { extend: 'csv', className: 'btn btn-sm' },--}}
{{--                { extend: 'excel', className: 'btn btn-sm' },--}}
{{--                { extend: 'print', className: 'btn btn-sm' }--}}
{{--            ]--}}
{{--        },--}}
{{--        "oLanguage": {--}}
{{--            "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },--}}
{{--            "sInfo": "Showing page _PAGE_ of _PAGES_",--}}
{{--            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',--}}
{{--            "sSearchPlaceholder": "Search...",--}}
{{--            "sLengthMenu": "Results :  _MENU_",--}}
{{--        },--}}
{{--        "stripeClasses": [],--}}
{{--        "lengthMenu": [7, 10, 20, 50],--}}
{{--        "pageLength": 7--}}
{{--    } );--}}
{{--</script>--}}
<script type="application/javascript">
    $(".toggle-password").click(function() {
        alert('yes');
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    $('.html5-extension').DataTable( {
        "dom": "<'dt--top-section'<'row'<'col-sm-12 col-md-6 d-flex justify-content-md-start justify-content-center'B><'col-sm-12 col-md-6 d-flex justify-content-md-end justify-content-center mt-md-0 mt-3'f>>>" +
            "<'table-responsive'tr>" +
            "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
        buttons: {
            buttons: [
                { extend: 'copy', className: 'btn btn-sm' },
                { extend: 'csv', className: 'btn btn-sm' },
                { extend: 'excel', className: 'btn btn-sm' },
                { extend: 'print', className: 'btn btn-sm' }
            ]
        },
        "oLanguage": {
            "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [7, 10, 20, 50],
        "pageLength": 7
    } );
    $(document).ready(function(){$(".zero-configuration").DataTable();var o=$(".row-grouping").DataTable({columnDefs:[{visible:!1,targets:2}],order:[[2,"asc"]],displayLength:10,drawCallback:function(a){var o=this.api(),t=o.rows({page:"current"}).nodes(),r=null;o.column(2,{page:"current"}).data().each(function(a,o){r!==a&&($(t).eq(o).before('<tr class="group"><td colspan="5">'+a+"</td></tr>"),r=a)})}});$(".row-grouping tbody").on("click","tr.group",function(){var a=o.order()[0];2===a[0]&&"asc"===a[1]?o.order([2,"desc"]).draw():o.order([2,"asc"]).draw()}),$(".complex-headers").DataTable();var a=$(".add-rows").DataTable(),t=2;$("#addRow").on("click",function(){a.row.add([t+".1",t+".2",t+".3",t+".4",t+".5"]).draw(!1),t++}),$(".dataex-html5-selectors").DataTable({dom:"Bfrtip",buttons:[{extend:"copyHtml5",exportOptions:{columns:[0,":visible"]}},{extend:"pdfHtml5",exportOptions:{columns:":visible"}},{text:"JSON",action:function(a,o,t,r){var e=o.buttons.exportData();$.fn.dataTable.fileSave(new Blob([JSON.stringify(e)]),"Export.json")}},{extend:"print",exportOptions:{columns:":visible"}}]}),$(".scroll-horizontal-vertical").DataTable({scrollY:200,scrollX:!0})});
</script>
<script src="{{ asset('backend/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
{{--<script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>--}}
{{--<script src="{{ asset('backend/plugins/editors/quill/quill.js')}}"></script>--}}
{{--<script src="{{ asset('backend/plugins/editors/quill/custom-quill.js')}}"></script>--}}
@livewireScripts
@stack('scripts')
</body>
</html>
