    @include('includes.header')
    @include('includes.top-nav')
	@include('includes.main-nav')
    @yield('content')
  
    @include('includes.quick_links')
	@include('includes.footer')
	@include('includes.scripts')
