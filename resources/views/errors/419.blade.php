<!DOCTYPE html>
<html lang="en">
<head>

    <title>@yield('title')</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png')}}">

    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/flaticon/font/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/animate.css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/slick-carousel/slick/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('../css/simditor.css') }}">

    @livewireStyles
</head>
<body style="background: #eadcd2 !important;">
<main id="content">
    <div class="container">
        <div class="space-bottom-1 space-top-xl-2 space-bottom-xl-4">
            <div class="d-flex flex-column align-items-center pt-lg-7 pb-lg-4 pb-lg-6">
                <div class="font-weight-medium font-size-200 font-size-xs-170 text-lh-sm mt-xl-1">419</div>
                <h6 class="font-size-4 font-weight-medium mb-2">Woops, page has expired</h6>
                <span class="font-size-2 mb-6">You could either go back or go to homepage</span>
                <div class="d-flex align-items-center flex-column">
                    <a class="btn btn-dark rounded-0 btn-wide height-60 width-250 font-weight-medium" href="{{'/'}}">Go Back</a>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-migrate/dist/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('assets/vendor/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/vendor/multilevel-sliding-mobile-menu/dist/jquery.zeynep.js') }}"></script>
<script src="{{ asset('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('assets/vendor/slick-carousel/slick/slick.min.js') }}"></script>

<script src="{{ asset('assets/js/hs.core.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.unfold.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.malihu-scrollbar.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.slick-carousel.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.show-animation.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.selectpicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('../js/module.js') }}"></script>
<script type="text/javascript" src="{{ asset('../js/hotkeys.js') }}"></script>
<script type="text/javascript" src="{{ asset('../js/uploader.js') }}"></script>
<script type="text/javascript" src="{{ asset('../js/simditor.js') }}"></script>


<script>
    $(document).on('ready', function () {
        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

        // initialization of malihu scrollbar
        $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

        // init zeynepjs
        var zeynep = $('.zeynep').zeynep({
            onClosed: function () {
                // enable main wrapper element clicks on any its children element
                $("body main").attr("style", "");

                console.log('the side menu is closed.');
            },
            onOpened: function () {
                // disable main wrapper element clicks on any its children element
                $("body main").attr("style", "pointer-events: none;");

                console.log('the side menu is opened.');
            }
        });

        // handle zeynep overlay click
        $(".zeynep-overlay").click(function () {
            zeynep.close();
        });

        // open side menu if the button is clicked
        $(".cat-menu").click(function () {
            if ($("html").hasClass("zeynep-opened")) {
                zeynep.close();
            } else {
                zeynep.open();
            }
        });
    });
</script>

<script>
    if($(location).attr('pathname') == '/DesignerRequestQuote'){
        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div>' +
                        '<input type="url" class="input-text form-control" name="mytext[]" id="first_name"  required><a href="#" class="remove_field label-blue"><i class="fas fa-minus"></i></a>' +
                        '</div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
    }
    if($(location).attr('pathname') == '/ReaderSignUp'){
        var myInput = document.getElementById("password");
        var letter = document.getElementById("letter");
        var capital = document.getElementById("capital");
        var number = document.getElementById("number");
        var length = document.getElementById("length");

        // When the user clicks on the password field, show the message box
        myInput.onfocus = function() {
            document.getElementById("message").style.display = "block";
        }

        // When the user clicks outside of the password field, hide the message box
        myInput.onblur = function() {
            document.getElementById("message").style.display = "none";
        }

        // When the user starts to type something inside the password field
        myInput.onkeyup = function() {
            // Validate lowercase letters
            var lowerCaseLetters = /[a-z]/g;
            if(myInput.value.match(lowerCaseLetters)) {
                letter.classList.remove("invalid");
                letter.classList.add("valid");
            } else {
                letter.classList.remove("valid");
                letter.classList.add("invalid");
            }

            // Validate capital letters
            var upperCaseLetters = /[A-Z]/g;
            if(myInput.value.match(upperCaseLetters)) {
                capital.classList.remove("invalid");
                capital.classList.add("valid");
            } else {
                capital.classList.remove("valid");
                capital.classList.add("invalid");
            }

            // Validate numbers
            var numbers = /[0-9]/g;
            if(myInput.value.match(numbers)) {
                number.classList.remove("invalid");
                number.classList.add("valid");
            } else {
                number.classList.remove("valid");
                number.classList.add("invalid");
            }

            // Validate length
            if(myInput.value.length >= 8) {
                length.classList.remove("invalid");
                length.classList.add("valid");
            } else {
                length.classList.remove("valid");
                length.classList.add("invalid");
            }
        }
        $(function() {
            //on keypress
            $('#password_confirmation').keyup(function(e) {
                //get values
                var password = $('#password').val();
                var confirm_password = $(this).val();

                //check the strings
                if (password == confirm_password) {
                    //if both are same remove the error and allow to submit
                    $('.passwordyes').text('Password are matching');
                    $('.passworderror').text('');
                } else {
                    //if not matching show error and not allow to submit
                    $('.passwordyes').text('');
                    $('.passworderror').text('Password not matching');
                }
            });
        });
    }
</script>
@livewireScripts
@stack('scripts')
</body>
</html>
