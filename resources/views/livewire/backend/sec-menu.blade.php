@push('styles')
    <style>
        .login_avatar{
            border-radius: 50%;
            position: relative;
            margin: 0 auto;
            display: block;
            margin-top: 10px;
        }
        .logout_button{
            margin: 0 auto;
            display: block;
            position: relative;
            text-align: center;
        }
    </style>
@endpush
<div>
    <div class="sidebar-wrapper sidebar-theme">
        <nav id="sidebar">
            <div class="shadow-bottom"></div>
            <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <img  class="login_avatar"  src="{{ asset('backend/assets/img/user.png') }}" style="-webkit-user-select: none;width: 50px">
                <p class="text-center" style="margin-top: 10px"><strong>{{auth()->user()->name}}</strong></p>
            </a>
            <a class="logout_button" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <svg style="margin-top: -5px;" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-power"><path d="M18.36 6.64a9 9 0 1 1-12.73 0"/><line x1="12" y1="2" x2="12" y2="12"/></svg>
                Sign Out
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </a>
            <a href="javascript:void(0);"  class="logout_button sidebarCollapse d-block d-sm-none d-md-block d-lg-none" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="list-unstyled menu-categories" id="accordionExample">
                <?php
                $role = Auth::user()->role;
                $checkrole = explode(',', $role);
                ?>

                @if((in_array('Admin', $checkrole)))
                        <li class="menu">
                            <a href="{{'/Admin/Dashboard'}}"  {{ Request::is('Admin/Dashboard') ? 'data-active=true' : '' }}  aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                    <span>Dashboard</span>
                                </div>
                            </a>
                        </li>
                        <li class="menu">
                            <a href="{{'/Admin/AdminUsers'}}"  {{ Request::is('Admin/AdminUsers') ? 'data-active=true' : '' }}  aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                                    <span>Users</span>
                                </div>
                            </a>
                        </li>
                        <li class="menu">
                            <a href="#palletsettings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-truck"><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>
                                    <span>Pallet location</span>
                                </div>
                                <div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                                </div>
                            </a>
                            <ul class="collapse submenu list-unstyled" id="palletsettings" data-parent="#accordionExample">
                                @foreach($pallet_menu as $menu)
                                    <li><a href="{{'/Admin/PalletTransfer'}}/{{$menu->inventory_location_name}}"> {{$menu->inventory_location_name}} </a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="menu">
                            <a href="#Management" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevrons-right"><polyline points="13 17 18 12 13 7"></polyline><polyline points="6 17 11 12 6 7"></polyline></svg>
                                    <span>Management</span>
                                </div>
                                <div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                                </div>
                            </a>
                            <ul class="collapse submenu list-unstyled" id="Management" data-parent="#accordionExample">
{{--                                @foreach($pallet_menu as $menu)--}}
{{--                                    --}}
{{--                                @endforeach--}}
                                <li><a href="{{'/Admin/InventoryLocation'}}"> Locations</a></li>
                                <li><a href="{{'/Admin/InventoryProducts'}}"> Products</a></li>
                                <li><a href="{{'/Admin/InventoryDistributors'}}"> Distributor</a></li>
                                <li><a href="{{'/Admin/InventoryTrucks'}}"> Trucks</a></li>
                            </ul>
                        </li>
                        <li class="menu">
                            <a href="{{'/Admin/PalletManagement'}}"  {{ Request::is('Admin/PalletManagement') ? 'data-active=true' : '' }}  aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                                    <span>Pallet Management</span>
                                </div>
                            </a>
                        </li>
                        <li class="menu">
                            <a href="{{'/Admin/UniversalSearch'}}"  {{ Request::is('Admin/UniversalSearch') ? 'data-active=true' : '' }}  aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                                    <span>Universal Search</span>
                                </div>
                            </a>
                        </li>
                        <li class="menu">
                            <a href="{{'/Admin/Reports'}}"  {{ Request::is('Admin/Reports') ? 'data-active=true' : '' }}  aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                                    <span>Reports</span>
                                </div>
                            </a>
                        </li>
                    @elseif(in_array('User', $checkrole) or in_array('Pallet Manufacturing', $checkrole) or in_array('Empties Yard', $checkrole) or
                            in_array('Packaging', $checkrole) or in_array('Pallet Repairs', $checkrole) or in_array('Scrapped Pallets', $checkrole) or
                            in_array('Finished Goods', $checkrole) or in_array('Distributor locations', $checkrole))
                        <li class="menu">
                            <a href="{{'/User/Dashboard'}}"  {{ Request::is('User/Dashboard') ? 'data-active=true' : '' }}  aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                    <span>Dashboard</span>
                                </div>
                            </a>
                        </li>
                        <li class="menu">
                            <a href="{{'/User/UserAccount'}}"  {{ Request::is('User/UserAccount') ? 'data-active=true' : '' }}  aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                                    <span>User Account</span>
                                </div>
                            </a>
                        </li>
                        <li class="menu">
                            <a href="#palletsettings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-truck"><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>
                                    <span>Pallet location</span>
                                </div>
                                <div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                                </div>
                            </a>
                            <ul class="collapse submenu list-unstyled" id="palletsettings" data-parent="#accordionExample">
                                <li><a href="{{'/User/PalletTransfer'}}/{{auth()->user()->role}}"> {{auth()->user()->role}} </a></li>
                            </ul>
                        </li>
                        @else
                        {{"Access denied!"}}
                        @endif
            </ul>
            <!-- <div class="shadow-bottom"></div> -->

        </nav>
    </div>
</div>
