@push('styles')
    <style>
        .alert-info {
            color: #fff;
            background-color: #2196f3;
            border-color: #2196f3;
            display: inline-block;
        }
        .more {
            padding: 6px;
            border-radius: 10px;
            padding: 6px;
            background: #3b3f5c !important;
            border: none;
            -webkit-transform: translateY(0);
            transform: translateY(0);
            box-shadow: 0 6px 10px 0 rgb(0 0 0 / 14%), 0 1px 18px 0 rgb(0 0 0 / 12%), 0 3px 5px -1px rgb(0 0 0 / 20%);
        }
    </style>
@endpush
<div>
    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">
        <!--  BEGIN SIDEBAR  -->
        <livewire:backend.sec-menu />
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                        <div class="widget widget-chart-one">
                            <div class="widget-heading">
                                <h5 class="">Pallets statistics</h5>
                                <ul class="tabs tab-pills">
                                    <li><a href="javascript:void(0);" id="tb_1" class="tabmenu">Monthly</a></li>
                                </ul>
                            </div>

                            <div class="widget-content">
                                <div class="tabs tab-content">
                                    <div id="content_1" class="tabcontent">
                                        <div id="revenueMonthly"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                        <div class="widget widget-three">
                            <div class="widget-heading">
                                <h5 class="">Transfer By scanning</h5>
                                <div class="w-icon">
                                    <div class="w-icon">
                                        @if(auth()->user()->role == "Admin")
                                            <a class="btn btn-primary more" href="{{'/Admin/ScanPallet'}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
                                        @elseif(auth()->user()->role == "User" or auth()->user()->role == "Pallet Manufacturing" or auth()->user()->role == "Empties Yard" or auth()->user()->role == "Packaging" or auth()->user()->role == "Pallet Repairs" or auth()->user()->role == "Scrapped Pallets" or auth()->user()->role == "Finished Goods" or auth()->user()->role == "Distributor locations")
                                            <a class="btn btn-primary more" href="{{'/User/ScanPallet'}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="order-summary">
                                    @foreach($Locations as $location)
                                        <div class="summary-list summary-income">
                                            <div class="summery-info">
                                                <div class="w-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
                                                </div>
                                                <div class="w-summary-details">
                                                    <div class="w-summary-info">
                                                        <h6>{{$location->inventory_location_name}} <span class="summary-count"></span></h6>
                                                        <p class="summary-average">{{$location->PalletNumbers}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                        <div class="widget widget-three">
                            <div class="widget-heading">
                                <h5 class="">Recently transferred pallet</h5>
                                <div class="w-icon">
                                    <a class="btn btn-primary more" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
                                </div>
                            </div>

                            <div class="widget-content">
                                <div class="table-responsive mb-4 mt-4">
                                    <div id="zero-config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="dataTables_length" id="zero-config_length">
                                                    <label>Results :
                                                        <select name="zero-config_length" class="form-control">
                                                            <option value="10">10</option>
                                                            <option value="25">25</option>
                                                            <option value="50">50</option>
                                                            <option value="100">100</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <div id="zero-config_filter" class="dataTables_filter">
                                                    <label>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle>
                                                            <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                                        </svg>
                                                        <input type="search" class="form-control" placeholder="Search..." aria-controls="zero-config">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="zero-config" class="table table-hover dataTable no-footer" style="width:100%" role="grid" aria-describedby="zero-config_info">
                                            <thead>
                                            <tr>
                                                <th>Barcode number</th>
                                                @if($pallet_name == "Pallet Manufacturing")
                                                    <th>Created By</th>
                                                    <th>Created at</th>
                                                    <th>Action</th>
                                                @else
                                                    <th>Transfer From</th>
                                                    @if($pallet_name == "Pallet Repairs")
                                                        <th>Number of repairs</th>
                                                    @endif
                                                    <th>Transferred By</th>
                                                    <th>Transferred date</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($Pallet_list as $pallet)
                                                <tr>
                                                    <td>{{$pallet->pallet_barcode}}</td>
                                                    @if($pallet_name == "Pallet Manufacturing")
                                                        <td>{{$pallet->name}}</td>
                                                        <td>{{$pallet->created_at}}</td>
                                                        <td>
                                                            <button style="float: right;" type="button" class="btn" data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $pallet->id }})">
                                                                Transfer
                                                            </button>
                                                            <svg wire:click="delete({{ $pallet->id }})"  onclick="return confirm('Are you sure you would like to delete record?');" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                        </td>
                                                    @else
                                                        <td>{{$pallet['TransferLocations']['from_location']}}</td>
                                                        @if($pallet_name == "Pallet Repairs")
                                                            <td>{{$pallet->PalletRepairs}}</td>
                                                        @endif
                                                        <td>{{$pallet['TransferLocations']['transfer_username']}}</td>
                                                        <td>{{$pallet['TransferLocations']['Transferred_date']}}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-5">
                                                <div class="dataTables_info" id="zero-config_info" role="status" aria-live="polite">
                                                    Showing page 1 of 1
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-7">
                                                <div class="dataTables_paginate paging_simple_numbers" id="zero-config_paginate">
                                                    <ul class="pagination">
                                                        <li class="paginate_button page-item previous disabled" id="zero-config_previous">
                                                            <a href="#" aria-controls="zero-config" data-dt-idx="0" tabindex="0" class="page-link">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline>
                                                                </svg>
                                                            </a>
                                                        </li>
                                                        <li class="paginate_button page-item active"><a href="#" aria-controls="zero-config" data-dt-idx="1" tabindex="0" class="page-link">1</a>
                                                        </li>
                                                        <li class="paginate_button page-item next disabled" id="zero-config_next"><a href="#" aria-controls="zero-config" data-dt-idx="2" tabindex="0" class="page-link"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <livewire:backend.footer />
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->
</div>
@push('scripts')
    <script type="text/javascript">
        window.livewire.on('userStore', () => {
            $('#createAccount').modal('hide');
        });
        window.livewire.on('userUpdate', () => {
            $('#updateModal').modal('hide');
        });
        try {

            Apex.tooltip = {
                theme: 'dark'
            }

            /*
                =================================
                    Revenue Monthly | Options
                =================================
            */
            var options1 = {
                chart: {
                    fontFamily: 'Nunito, sans-serif',
                    height: 365,
                    type: 'area',
                    zoom: {
                        enabled: false
                    },
                    dropShadow: {
                        enabled: true,
                        opacity: 0.3,
                        blur: 5,
                        left: -7,
                        top: 22
                    },
                    toolbar: {
                        show: false
                    },
                    events: {
                        mounted: function(ctx, config) {
                            const highest1 = ctx.getHighestValueInSeries(0);
                            const highest2 = ctx.getHighestValueInSeries(1);
                            const highest3 = ctx.getHighestValueInSeries(2);

                            ctx.addPointAnnotation({
                                x: new Date(ctx.w.globals.seriesX[0][ctx.w.globals.series[0].indexOf(highest1)]).getTime(),
                                y: highest1,
                                label: {
                                    style: {
                                        cssClass: 'd-none'
                                    }
                                },
                                customSVG: {
                                    SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#1b55e2" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
                                    cssClass: undefined,
                                    offsetX: -8,
                                    offsetY: 5
                                }
                            })

                            ctx.addPointAnnotation({
                                x: new Date(ctx.w.globals.seriesX[1][ctx.w.globals.series[1].indexOf(highest2)]).getTime(),
                                y: highest2,
                                label: {
                                    style: {
                                        cssClass: 'd-none'
                                    }
                                },
                                customSVG: {
                                    SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#e7515a" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
                                    cssClass: undefined,
                                    offsetX: -8,
                                    offsetY: 5
                                }
                            })

                            ctx.addPointAnnotation({
                                x: new Date(ctx.w.globals.seriesX[2][ctx.w.globals.series[2].indexOf(highest3)]).getTime(),
                                y: highest3,
                                label: {
                                    style: {
                                        cssClass: 'd-none'
                                    }
                                },
                                customSVG: {
                                    SVG: '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="#e2a03f" stroke="#fff" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg>',
                                    cssClass: undefined,
                                    offsetX: -8,
                                    offsetY: 5
                                }
                            })
                        },
                    }
                },
                colors: ['#1b6f6f', '#e7515a','#e2a03f'],
                dataLabels: {
                    enabled: false
                },
                markers: {
                    discrete: [{
                        seriesIndex: 0,
                        dataPointIndex: 7,
                        fillColor: '#000',
                        strokeColor: '#000',
                        size: 5
                    }, {
                        seriesIndex: 1,
                        dataPointIndex: 11,
                        fillColor: '#000',
                        strokeColor: '#000',
                        size: 4
                    },{
                        seriesIndex: 2,
                        dataPointIndex: 11,
                        fillColor: '#000',
                        strokeColor: '#000',
                        size: 4
                    }]
                },
                subtitle: {
                    text: 'Total number of pallet',
                    align: 'left',
                    margin: 0,
                    offsetX: -10,
                    offsetY: 35,
                    floating: false,
                    style: {
                        fontSize: '14px',
                        color:  '#888ea8'
                    }
                },
                title: {
                    text: {{$pallet_number}},
                    align: 'left',
                    margin: 0,
                    offsetX: -10,
                    offsetY: 0,
                    floating: false,
                    style: {
                        fontSize: '25px',
                        color:  '#bfc9d4'
                    },
                },
                stroke: {
                    show: true,
                    curve: 'smooth',
                    width: 3,
                    lineCap: 'square'
                },
                series: [{
                    name: 'Pallets',
                    data: ["434", "454", "232", "542"]
                }],
                labels: ['Jan - Mar', 'Apr - Jun', 'Jul - Sep', 'Oct - Dec'],
                xaxis: {
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false
                    },

                    crosshairs: {
                        show: true
                    },
                    labels: {
                        offsetX: 0,
                        offsetY: 5,
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Nunito, sans-serif',
                            cssClass: 'apexcharts-xaxis-title',
                        },
                    }
                },
                yaxis: {
                    labels: {
                        formatter: function(value, index) {
                            return (value)
                        },
                        offsetX: -22,
                        offsetY: 0,
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Nunito, sans-serif',
                            cssClass: 'apexcharts-yaxis-title',
                        },
                    }
                },
                grid: {
                    borderColor: '#191e3a',
                    strokeDashArray: 5,
                    xaxis: {
                        lines: {
                            show: true
                        }
                    },
                    yaxis: {
                        lines: {
                            show: false,
                        }
                    },
                    padding: {
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: -10
                    },
                },
                legend: {
                    position: 'top',
                    horizontalAlign: 'right',
                    offsetY: -50,
                    fontSize: '16px',
                    fontFamily: 'Nunito, sans-serif',
                    markers: {
                        width: 10,
                        height: 10,
                        strokeWidth: 0,
                        strokeColor: '#fff',
                        fillColors: undefined,
                        radius: 12,
                        onClick: undefined,
                        offsetX: 0,
                        offsetY: 0
                    },
                    itemMargin: {
                        horizontal: 0,
                        vertical: 20
                    }
                },
                tooltip: {
                    theme: 'dark',
                    marker: {
                        show: true,
                    },
                    x: {
                        show: false,
                    }
                },
                fill: {
                    type:"gradient",
                    gradient: {
                        type: "vertical",
                        shadeIntensity: 1,
                        inverseColors: !1,
                        opacityFrom: .28,
                        opacityTo: .05,
                        stops: [45, 100]
                    }
                },
                responsive: [{
                    breakpoint: 575,
                    options: {
                        legend: {
                            offsetY: -30,
                        },
                    },
                }]
            }

            /*
                ==================================
                    Sales By Category | Options
                ==================================
            */
            var options = {
                chart: {
                    type: 'donut',
                    width: 380
                },
                colors: ['#5c1ac3', '#e2a03f', '#e7515a', '#e2a03f'],
                dataLabels: {
                    enabled: false
                },
                legend: {
                    position: 'bottom',
                    horizontalAlign: 'center',
                    fontSize: '14px',
                    markers: {
                        width: 10,
                        height: 10,
                    },
                    itemMargin: {
                        horizontal: 0,
                        vertical: 8
                    }
                },
                plotOptions: {
                    pie: {
                        donut: {
                            size: '65%',
                            background: 'transparent',
                            labels: {
                                show: true,
                                name: {
                                    show: true,
                                    fontSize: '29px',
                                    fontFamily: 'Nunito, sans-serif',
                                    color: undefined,
                                    offsetY: -10
                                },
                                value: {
                                    show: true,
                                    fontSize: '26px',
                                    fontFamily: 'Nunito, sans-serif',
                                    color: '#bfc9d4',
                                    offsetY: 16,
                                    formatter: function (val) {
                                        return val
                                    }
                                },
                                total: {
                                    show: true,
                                    showAlways: true,
                                    label: 'Total',
                                    color: '#888ea8',
                                    formatter: function (w) {
                                        return w.globals.seriesTotals.reduce( function(a, b) {
                                            return a + b
                                        }, 0)
                                    }
                                }
                            }
                        }
                    }
                },
                stroke: {
                    show: true,
                    width: 25,
                    colors: '#0e1726'
                },
                series: [987895, 737, 270],
                labels: ['Apparel', 'Electronic', 'Others'],
                responsive: [{
                    breakpoint: 1599,
                    options: {
                        chart: {
                            width: '350px',
                            height: '400px'
                        },
                        legend: {
                            position: 'bottom'
                        }
                    },

                    breakpoint: 1439,
                    options: {
                        chart: {
                            width: '250px',
                            height: '390px'
                        },
                        legend: {
                            position: 'bottom'
                        },
                        plotOptions: {
                            pie: {
                                donut: {
                                    size: '65%',
                                }
                            }
                        }
                    },
                }]
            }


            /*
                ================================
                    Revenue Monthly | Render
                ================================
            */
            var chart1 = new ApexCharts(
                document.querySelector("#revenueMonthly"),
                options1
            );

            chart1.render();


        } catch(e) {
            console.log(e);
        }

    </script>
@endpush
