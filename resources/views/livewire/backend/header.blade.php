<div>
    <!--  BEGIN NAVBAR  -->
    <div class="header-container fixed-top">
        <header class="header navbar navbar-expand-sm">

            <ul class="navbar-item theme-brand flex-row  text-center">
                @if(auth()->user()->role == "Admin")
                <li class="nav-item theme-logo">

                    <a href="{{'/Admin/Dashboard'}}">
                        <img src="{{asset('backend/assets/img/logo_icon.png')}}" class="navbar-logo" alt="logo">
                    </a>
                </li>
                <li class="nav-item theme-text">
                    <a href="{{'/Admin/Dashboard'}}" class="nav-link"> SKOL</a>
                </li>
                @elseif(auth()->user()->role == "Artist")
                    <li class="nav-item theme-logo">

                        <a href="{{'/Artist/Dashboard'}}">
                            <img src="{{asset('frontend/assets/img/logo.png')}}" class="navbar-logo" alt="logo">
                        </a>
                    </li>
                    <li class="nav-item theme-text">
                        <a href="{{'/Artist/Dashboard'}}" class="nav-link"> MNI</a>
                    </li>
                @elseif(auth()->user()->role == "Competition")
                    <li class="nav-item theme-logo">

                        <a href="{{'/Competition/Dashboard'}}">
                            <img src="{{asset('frontend/assets/img/logo.png')}}" class="navbar-logo" alt="logo">
                        </a>
                    </li>
                    <li class="nav-item theme-text">
                        <a href="{{'/Competition/Dashboard'}}" class="nav-link"> MNI</a>
                    </li>
                @endif
            </ul>


            <ul class="navbar-item flex-row ml-md-auto">
                <li class="nav-item dropdown user-profile-dropdown">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img style="-webkit-user-select: none;"  class="login_avatar"  src="{{ asset('backend/assets/img/user.png') }}">
                    </a>
                </li>

            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->

    <!--  BEGIN NAVBAR  -->
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

{{--            <ul class="navbar-nav flex-row">--}}
{{--                <li>--}}
{{--                    <div class="page-header">--}}

{{--                        <nav class="breadcrumb-one" aria-label="breadcrumb">--}}
{{--                            <ol class="breadcrumb">--}}
{{--                                <li class="breadcrumb-item"><a href="javascript:void(0);">Menu</a></li>--}}
{{--                            </ol>--}}
{{--                        </nav>--}}

{{--                    </div>--}}
{{--                </li>--}}
{{--            </ul>--}}
            <ul class="navbar-nav flex-row ml-auto ">
                <li class="nav-item more-dropdown">
                    <div class="dropdown  custom-dropdown-icon">
                        <a class="dropdown-toggle btn" href="#" role="button" id="customDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>Settings</span> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></a>

{{--                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="customDropdown">--}}
{{--                            <a class="dropdown-item" data-value="Settings" href="javascript:void(0);">Settings</a>--}}
{{--                            <a class="dropdown-item" data-value="Mail" href="javascript:void(0);">Mail</a>--}}
{{--                            <a class="dropdown-item" data-value="Print" href="javascript:void(0);">Print</a>--}}
{{--                            <a class="dropdown-item" data-value="Download" href="javascript:void(0);">Download</a>--}}
{{--                            <a class="dropdown-item" data-value="Share" href="javascript:void(0);">Share</a>--}}
{{--                        </div>--}}
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
</div>
