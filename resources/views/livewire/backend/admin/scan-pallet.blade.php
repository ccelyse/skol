@push('styles')
    <style>
        .alert-success{
            color: #fff;
            background-color: #d5252a;
            border-color: #d5252a;
            display: table-cell;
        }
        .widget-content-area {
            -webkit-box-shadow: 0 6px 10px 0 rgba(0,0,0,.14), 0 1px 18px 0 rgba(0,0,0,.12), 0 3px 5px -1px rgba(0,0,0,.2);
            -moz-box-shadow: 0 6px 10px 0 rgba(0,0,0,.14), 0 1px 18px 0 rgba(0,0,0,.12), 0 3px 5px -1px rgba(0,0,0,.2);
            box-shadow: 0 6px 10px 0 rgba(0,0,0,.14), 0 1px 18px 0 rgba(0,0,0,.12), 0 3px 5px -1px rgba(0,0,0,.2);
        }

        .no-content:before, .no-content:after { display: none!important; }
        .new-control.new-checkbox .new-control-indicator {
            top: 0;
            height: 22px;
            width: 22px;
            border-radius: 50%;
        }

    </style>
@endpush
<div>
    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">
        <!--  BEGIN SIDEBAR  -->
        <livewire:backend.sec-menu />
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        @if (session('success_delete'))
                            <div class="alert alert-success mb-4" role="alert" style="width: 100%; text-align: center;margin: 0 auto;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Success!</strong> {{ session('success_delete') }}
                            </div>
                        @endif
                            @if(auth()->user()->role == "Admin")
                                <a href="{{'/Admin/ScanPallet'}}" style="float: left;" class="btn">Add another pallet</a>
                            @elseif(auth()->user()->role == "User" or auth()->user()->role == "Pallet Manufacturing" or auth()->user()->role == "Empties Yard" or auth()->user()->role == "Packaging" or auth()->user()->role == "Pallet Repairs" or auth()->user()->role == "Scrapped Pallets" or auth()->user()->role == "Finished Goods" or auth()->user()->role == "Distributor locations")
                                <a href="{{'/User/ScanPallet'}}" style="float: left;" class="btn">Add another pallet</a>
                            @endif

                            @if(auth()->user()->role == "Admin")
                                <a href="{{'/Admin/PendingTransfers'}}" style="float: right;" class="btn">Check all pending transfers</a>
                            @elseif(auth()->user()->role == "User")
                                <a href="{{'/User/PendingTransfers'}}" style="float: right;" class="btn">Pending Transfers</a>
                            @endif
                    </div>
                </div>
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                            @if($pallet_title)
                                <h3 style="text-align: center">{{$pallet_title}}</h3>
                            @else
                                <div class="table-responsive mb-4 mt-4">
                                    <div id="zero-config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="dataTables_length" id="zero-config_length">
                                                    <label>Results :
                                                        <select name="zero-config_length" class="form-control">
                                                            <option value="10">10</option>
                                                            <option value="25">25</option>
                                                            <option value="50">50</option>
                                                            <option value="100">100</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <div id="zero-config_filter" class="dataTables_filter">
                                                    <label>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle>
                                                            <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                                        </svg>
                                                        <input type="search" class="form-control" placeholder="Search..." aria-controls="zero-config">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="zero-config" class="table table-hover dataTable no-footer" style="width:100%" role="grid" aria-describedby="zero-config_info">
                                            <thead>
                                            <tr>
                                                <th>Barcode number</th>
                                                @if($pallet_name == "Pallet Manufacturing")
                                                    <th>Created By</th>
                                                    <th>Created at</th>
                                                    <th>Action</th>
                                                @else
                                                    <th>Transfer From</th>
                                                    <th>Transfer To</th>
                                                    @if($pallet_name == "Pallet Repairs")
                                                        <th>Number of repairs</th>
                                                    @endif
                                                    <th>Transferred By</th>
                                                    <th>Transferred date</th>
                                                    <th>Action</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($Pallet_list as $pallet)
                                                <tr>
                                                    <td>{{$pallet->pallet_barcode}}</td>
                                                    @if($pallet_name == "Pallet Manufacturing")
                                                        <td>{{$pallet->name}}</td>
                                                        <td>{{$pallet->created_at}}</td>
                                                        <td>
                                                            <button style="float: right;" type="button" class="btn" wire:click="addList({{ $pallet->id }})">
                                                                add on the list ()
                                                            </button>
                                                        </td>
                                                        <td>
{{--                                                            <button style="float: right;" type="button" class="btn" data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $pallet->id }})">--}}
{{--                                                                Transfer--}}
{{--                                                            </button>--}}
                                                            @if(auth()->user()->role == "Admin")
                                                                <svg wire:click="delete({{ $pallet->id }})"  onclick="return confirm('Are you sure you would like to delete record?');" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                            @endif
                                                        </td>
                                                    @else
                                                        <td>{{$pallet['TransferLocations']['from_location']}}</td>
                                                        <td>{{$pallet['TransferLocations']['current_location']}}</td>
                                                        @if($pallet_name == "Pallet Repairs")
                                                            <td>{{$pallet->PalletRepairs}}</td>
                                                        @endif
                                                        <td>{{$pallet['TransferLocations']['transfer_username']}}</td>
                                                        <td>{{$pallet['TransferLocations']['Transferred_date']}}</td>
                                                        <td>
                                                            <button style="float: right;" type="button" class="btn" wire:click="addList({{ $pallet->id }})">
                                                                add on the list ()
                                                            </button>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-5">
                                                <div class="dataTables_info" id="zero-config_info" role="status" aria-live="polite">
                                                    Showing page 1 of 1
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-7">
                                                <div class="dataTables_paginate paging_simple_numbers" id="zero-config_paginate">
                                                    <ul class="pagination">
                                                        <li class="paginate_button page-item previous disabled" id="zero-config_previous">
                                                            <a href="#" aria-controls="zero-config" data-dt-idx="0" tabindex="0" class="page-link">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline>
                                                                </svg>
                                                            </a>
                                                        </li>
                                                        <li class="paginate_button page-item active"><a href="#" aria-controls="zero-config" data-dt-idx="1" tabindex="0" class="page-link">1</a>
                                                        </li>
                                                        <li class="paginate_button page-item next disabled" id="zero-config_next"><a href="#" aria-controls="zero-config" data-dt-idx="2" tabindex="0" class="page-link"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            <livewire:backend.footer />
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->
</div>
@push('scripts')
    <script type="text/javascript">
        window.livewire.on('userStore', () => {
            $('#createInventory').modal('hide');
        });
        window.livewire.on('userUpdate', () => {
            $('#updateModal').modal('hide');
        });
    </script>
@endpush
