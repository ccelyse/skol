@push('styles')
    <style>
        .alert-success{
            color: #fff;
            background-color: #d5252a;
            border-color: #d5252a;
            display: table-cell;
        }
        .widget-content-area {
            -webkit-box-shadow: 0 6px 10px 0 rgba(0,0,0,.14), 0 1px 18px 0 rgba(0,0,0,.12), 0 3px 5px -1px rgba(0,0,0,.2);
            -moz-box-shadow: 0 6px 10px 0 rgba(0,0,0,.14), 0 1px 18px 0 rgba(0,0,0,.12), 0 3px 5px -1px rgba(0,0,0,.2);
            box-shadow: 0 6px 10px 0 rgba(0,0,0,.14), 0 1px 18px 0 rgba(0,0,0,.12), 0 3px 5px -1px rgba(0,0,0,.2);
        }

        .no-content:before, .no-content:after { display: none!important; }
        .new-control.new-checkbox .new-control-indicator {
            top: 0;
            height: 22px;
            width: 22px;
            border-radius: 50%;
        }
        /* The container */
        .container_check {
            display: block;
            position: relative;
            padding-left: 20px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container_check input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container_check:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container_check input:checked ~ .checkmark {
            background-color: #2196F3;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container_check input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container_check .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>
@endpush
<div>
    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">
        <!--  BEGIN SIDEBAR  -->
        <livewire:backend.sec-menu />
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <h2 style="margin-left: 10px;">{{$pallet_name}}</h2>
                        @if (session('success_delete'))
                            <div class="alert alert-success mb-4" role="alert" style="width: 100%; text-align: center;margin: 0 auto;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Success!</strong> {{ session('success_delete') }}
                            </div>
                        @endif
                            <button style="float: left;" type="button" class="btn" data-toggle="modal" data-target="#updateModal_selection">
                                Transfer by selection
                            </button>
                        @if(auth()->user()->role == "Admin")
                            <a href="{{'/Admin/ScanPallet'}}" style="float: right;" class="btn">Transfer By scanning</a>
                        @elseif(auth()->user()->role == "User")
                                <a href="{{'/User/ScanPallet'}}" style="float: right;" class="btn">Transfer By scanning</a>
                        @endif
                    </div>
                </div>
                <div wire:ignore.self class="modal fade" id="updateModal_selection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Pallet Transfer</h5>
                            </div>
                            <div class="modal-body">
                                <div class="widget-content widget-content-area">
                                    <form>
                                        @if (session('success_error'))
                                            <div class="alert alert-success mb-4" role="alert" style="width: 100%; text-align: center;margin: 0 auto;">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                </button>
                                                <strong>Success!</strong> {{ session('success_error') }}
                                            </div>
                                        @endif
                                        <div class="row mb-4">
                                            <div class="col">
                                                <label>Pallet location</label>
                                                <select class="form-control" wire:model="pallet_location_id_transfer" required>
                                                    <option>Select Pallet location</option>
                                                    @foreach($InventoryLocation as $location)
                                                        <option value="{{$location->id}}">{{$location->inventory_location_name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('pallet_location_id_transfer') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        @if($pallet_name == "Finished Goods")
                                            <div class="row mb-4">
                                                <div class="col">
                                                    <label>Distributor Name</label>
                                                    <select class="form-control" wire:model="pallet_distributor" required>
                                                        <option>Select Distributor Name</option>
                                                        @foreach($InventoryDistributor as $distributor)
                                                            <option value="{{$distributor->id}}">{{$distributor->distributor_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('pallet_distributor') <span class="text-danger error">{{ $message }}</span>@enderror
                                                </div>
                                            </div>
                                        @endif
                                        <div class="row mb-4">
                                            <div class="col">
                                                <label>Pallet number (from) Ex:SKL30</label>
                                                <input class="form-control" wire:model="pallet_number_from" required>
                                                @error('pallet_number_from') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                            <div class="col">
                                                <label>Pallet number (to) Ex:SKL50</label>
                                                <input class="form-control" wire:model="pallet_number_to" required>
                                                @error('pallet_number_to') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button wire:click.prevent="TransferSelected()"  class="btn btn-primary"><i class="flaticon-cancel-12"></i> Submit</button>
                                <button wire:click.prevent="cancel()"  class="btn btn-primary" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                            <table class="table zero-configuration" style="width:100%">
                                <thead>
                                <tr>
                                    {{--                                            <th></th>--}}
                                    <th>Barcode number</th>
                                    @if($pallet_name == "Pallet Manufacturing")
                                        {{--                                                <th>Pallet Location</th>--}}
                                        <th>Created By</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    @else
                                        <th>Transfer From</th>
                                        @if($pallet_name == "Pallet Repairs")
                                            <th>Number of repairs</th>
                                        @endif
                                        <th>Transferred By</th>
                                        <th>Transferred date</th>
                                        @if($pallet_name == "Distributor locations")
                                            <th>Distributor name</th>
                                        @endif
                                        <th>Action</th>
                                    @endif
                                    {{--
                                    {{--                                            @if($pallet_name !== "Pallet Manufacturing")--}}
                                    {{--                                            <th>Pallet Products</th>--}}
                                    {{--                                            @endif--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Pallet_list as $pallet)
                                    <tr>
                                        {{--                                                <td>--}}
                                        {{--                                                    <label class="container_check">--}}
                                        {{--                                                        <input type="checkbox" wire:model="selected" value="{{ $pallet->id }}">--}}
                                        {{--                                                        <span class="checkmark"></span>--}}
                                        {{--                                                    </label>--}}
                                        {{--                                                </td>--}}
                                        <td>{{$pallet->pallet_barcode}}</td>
                                        @if($pallet_name == "Pallet Manufacturing")
                                            {{--                                                    <td>{{$pallet->inventory_location_name}}</td>--}}
                                            <td>{{$pallet->name}}</td>
                                            <td>{{$pallet->created_at}}</td>
                                            <td>
{{--                                                <button style="float: right;" type="button" class="btn" data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $pallet->id }})">--}}
{{--                                                    Transfer--}}
{{--                                                </button>--}}
                                                @if(auth()->user()->role == "Admin")
                                                    <svg wire:click="delete({{ $pallet->id }})"  onclick="return confirm('Are you sure you would like to delete record?');" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                @endif
                                            </td>
                                        @else
                                            <td>@if(empty($pallet['TransferLocations'])) @else {{$pallet['TransferLocations']['from_location']}} @endif</td>
                                            @if($pallet_name == "Pallet Repairs")
                                                <td>{{$pallet->PalletRepairs}}</td>
                                            @endif
                                            <td>@if(empty($pallet['TransferLocations'])) @else {{$pallet['TransferLocations']['transfer_username']}} @endif</td>
                                            <td>@if(empty($pallet['TransferLocations'])) @else {{$pallet['TransferLocations']['Transferred_date']}} @endif</td>
                                            @if($pallet_name == "Distributor locations")
                                                <td>@if(empty($pallet['DistributorName'])) @else {{$pallet['DistributorName']['distributor_name']}} @endif</td>
                                            @endif
                                            <td>
{{--                                                <button style="float: right;" type="button" class="btn" data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $pallet->id }})">--}}
{{--                                                    Transfer--}}
{{--                                                </button>--}}
                                                @if(auth()->user()->role == "Admin")
                                                    <svg wire:click="delete({{ $pallet->id }})"  onclick="return confirm('Are you sure you would like to delete record?');" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
{{--                            <div class="table-responsive mb-4 mt-4">--}}
{{--                                <div id="zero-config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-sm-12 col-md-6">--}}
{{--                                            <div class="dataTables_length" id="zero-config_length">--}}
{{--                                                <label>Results :--}}
{{--                                                    <select name="zero-config_length" class="form-control">--}}
{{--                                                        <option value="10">10</option>--}}
{{--                                                        <option value="25">25</option>--}}
{{--                                                        <option value="50">50</option>--}}
{{--                                                        <option value="100">100</option>--}}
{{--                                                    </select>--}}
{{--                                                </label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-sm-12 col-md-6">--}}
{{--                                            <div id="zero-config_filter" class="dataTables_filter">--}}
{{--                                                <label>--}}
{{--                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle>--}}
{{--                                                        <line x1="21" y1="21" x2="16.65" y2="16.65"></line>--}}
{{--                                                    </svg>--}}
{{--                                                    <input type="search" class="form-control" wire:model="searchColumnsBarcode" placeholder="Search..." aria-controls="zero-config">--}}
{{--                                                </label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <table id="zero-config" class="table table-hover dataTable no-footer" style="width:100%" role="grid" aria-describedby="zero-config_info">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--                                            <th></th>--}}
{{--                                            <th>Barcode number</th>--}}
{{--                                            @if($pallet_name == "Pallet Manufacturing")--}}
{{--                                                <th>Pallet Location</th>--}}
{{--                                                <th>Created By</th>--}}
{{--                                                <th>Created at</th>--}}
{{--                                                <th>Action</th>--}}
{{--                                            @else--}}
{{--                                                <th>Transfer From</th>--}}
{{--                                                @if($pallet_name == "Pallet Repairs")--}}
{{--                                                <th>Number of repairs</th>--}}
{{--                                                @endif--}}
{{--                                                <th>Transferred By</th>--}}
{{--                                                <th>Transferred date</th>--}}
{{--                                                @if($pallet_name == "Distributor locations")--}}
{{--                                                    <th>Distributor name</th>--}}
{{--                                                @endif--}}
{{--                                                <th>Action</th>--}}
{{--                                            @endif--}}
{{----}}
{{--                                            @if($pallet_name !== "Pallet Manufacturing")--}}
{{--                                            <th>Pallet Products</th>--}}
{{--                                            @endif--}}
{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}
{{--                                        @foreach($Pallet_list as $pallet)--}}
{{--                                            <tr>--}}
{{--                                                <td>--}}
{{--                                                    <label class="container_check">--}}
{{--                                                        <input type="checkbox" wire:model="selected" value="{{ $pallet->id }}">--}}
{{--                                                        <span class="checkmark"></span>--}}
{{--                                                    </label>--}}
{{--                                                </td>--}}
{{--                                                <td>{{$pallet->pallet_barcode}}</td>--}}
{{--                                                    @if($pallet_name == "Pallet Manufacturing")--}}
{{--    --}}{{--                                                    <td>{{$pallet->inventory_location_name}}</td>--}}
{{--                                                        <td>{{$pallet->name}}</td>--}}
{{--                                                        <td>{{$pallet->created_at}}</td>--}}
{{--                                                        <td>--}}
{{--                                                            <button style="float: right;" type="button" class="btn" data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $pallet->id }})">--}}
{{--                                                                Transfer--}}
{{--                                                            </button>--}}
{{--                                                            @if(auth()->user()->role == "Admin")--}}
{{--                                                                <svg wire:click="delete({{ $pallet->id }})"  onclick="return confirm('Are you sure you would like to delete record?');" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>--}}
{{--                                                            @endif--}}
{{--                                                        </td>--}}
{{--                                                    @else--}}
{{--                                                    <td>@if(empty($pallet['TransferLocations'])) @else {{$pallet['TransferLocations']['from_location']}} @endif</td>--}}
{{--                                                    @if($pallet_name == "Pallet Repairs")--}}
{{--                                                    <td>{{$pallet->PalletRepairs}}</td>--}}
{{--                                                    @endif--}}
{{--                                                    <td>@if(empty($pallet['TransferLocations'])) @else {{$pallet['TransferLocations']['transfer_username']}} @endif</td>--}}
{{--                                                    <td>@if(empty($pallet['TransferLocations'])) @else {{$pallet['TransferLocations']['Transferred_date']}} @endif</td>--}}
{{--                                                    @if($pallet_name == "Distributor locations")--}}
{{--                                                    <td>@if(empty($pallet['DistributorName'])) @else {{$pallet['DistributorName']['distributor_name']}} @endif</td>--}}
{{--                                                    @endif--}}
{{--                                                    <td>--}}
{{--                                                        <button style="float: right;" type="button" class="btn" data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $pallet->id }})">--}}
{{--                                                            Transfer--}}
{{--                                                        </button>--}}
{{--                                                        @if(auth()->user()->role == "Admin")--}}
{{--                                                        <svg wire:click="delete({{ $pallet->id }})"  onclick="return confirm('Are you sure you would like to delete record?');" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>--}}
{{--                                                        @endif--}}
{{--                                                    </td>--}}
{{--                                                @endif--}}
{{--                                            </tr>--}}
{{--                                        @endforeach--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-sm-12 col-md-5">--}}
{{--                                            <div class="dataTables_info" id="zero-config_info" role="status" aria-live="polite">--}}
{{--                                                Showing page 1 of 1--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-sm-12 col-md-7">--}}
{{--                                            <div class="dataTables_paginate paging_simple_numbers" id="zero-config_paginate">--}}
{{--                                                <ul class="pagination">--}}
{{--                                                    <li class="paginate_button page-item previous disabled" id="zero-config_previous">--}}
{{--                                                        <a href="#" aria-controls="zero-config" data-dt-idx="0" tabindex="0" class="page-link">--}}
{{--                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline>--}}
{{--                                                            </svg>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="paginate_button page-item active"><a href="#" aria-controls="zero-config" data-dt-idx="1" tabindex="0" class="page-link">1</a>--}}
{{--                                                    </li>--}}
{{--                                                    <li class="paginate_button page-item next disabled" id="zero-config_next"><a href="#" aria-controls="zero-config" data-dt-idx="2" tabindex="0" class="page-link"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a></li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--                                        <div class="modal-dialog" role="document">--}}
{{--                                            <div class="modal-content">--}}
{{--                                                <div class="modal-header">--}}
{{--                                                    <h5 class="modal-title" id="exampleModalLabel">Pallet Transfer</h5>--}}
{{--                                                </div>--}}
{{--                                                <div class="modal-body">--}}
{{--                                                    <div class="widget-content widget-content-area">--}}
{{--                                                        <form>--}}
{{--                                                            <div class="row mb-4">--}}
{{--                                                                <div class="col">--}}
{{--                                                                    <label>Pallet location</label>--}}
{{--                                                                    <select class="form-control" wire:model="pallet_location_id_transfer" required>--}}
{{--                                                                        <option>Select Pallet location</option>--}}
{{--                                                                        @foreach($InventoryLocation as $location)--}}
{{--                                                                            <option value="{{$location->id}}">{{$location->inventory_location_name}}</option>--}}
{{--                                                                        @endforeach--}}
{{--                                                                    </select>--}}
{{--                                                                    @error('pallet_location_id') <span class="text-danger error">{{ $message }}</span>@enderror--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                            @if($pallet_name == "Finished Goods")--}}
{{--                                                            <div class="row mb-4">--}}
{{--                                                                <div class="col">--}}
{{--                                                                    <label>Distributor Name</label>--}}
{{--                                                                    <select class="form-control" wire:model="pallet_distributor" required>--}}
{{--                                                                        <option>Select Distributor Name</option>--}}
{{--                                                                        @foreach($InventoryDistributor as $distributor)--}}
{{--                                                                            <option value="{{$distributor->id}}">{{$distributor->distributor_name}}</option>--}}
{{--                                                                        @endforeach--}}
{{--                                                                    </select>--}}
{{--                                                                    @error('pallet_distributor') <span class="text-danger error">{{ $message }}</span>@enderror--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                            @endif--}}
{{--                                                            @if($pallet_name !== "Pallet Manufacturing")--}}
{{--                                                                @foreach($products_ids as $index => $products)--}}
{{--                                                                    <div class="row mb-4">--}}
{{--                                                                        <div class="col">--}}
{{--                                                                            <label>Pallet products</label>--}}
{{--                                                                            <select class="form-control" readonly="">--}}
{{--                                                                                <option value="{{$products->product_id}}">{{$products->product_name}}</option>--}}
{{--                                                                            </select>--}}
{{--                                                                            <svg wire:click="PalletRemove({{ $products->product_id }})"  onclick="return confirm('Are you sure you would like to delete record?');" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle table-cancel"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                @endforeach--}}
{{--                                                                <div class="row mb-4">--}}
{{--                                                                    <div class="col">--}}
{{--                                                                        <label>Pallet products</label>--}}
{{--                                                                        <select class="form-control" wire:model="pallet_product_id.0" required>--}}
{{--                                                                            <option>Select Pallet products</option>--}}
{{--                                                                            @foreach($Inventory_Products as $products)--}}
{{--                                                                                <option value="{{$products->id}}">{{$products->product_name}}</option>--}}
{{--                                                                            @endforeach--}}
{{--                                                                        </select>--}}
{{--                                                                        @error('pallet_product_id') <span class="text-danger error">{{ $message }}</span>@enderror--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                                @foreach($inputs as $key => $value)--}}
{{--                                                                    <div class=" add-input" style="padding-bottom: 10px;">--}}
{{--                                                                        <div class="row mb-4">--}}
{{--                                                                            <div class="col">--}}
{{--                                                                                <label>Pallet products</label>--}}
{{--                                                                                <select class="form-control" wire:model="pallet_product_id.{{ $value }}" required>--}}
{{--                                                                                    <option>Select Pallet location</option>--}}
{{--                                                                                    @foreach($Inventory_Products as $products)--}}
{{--                                                                                        <option value="{{$products->id}}">{{$products->product_name}}</option>--}}
{{--                                                                                    @endforeach--}}
{{--                                                                                </select>--}}
{{--                                                                                @error('name.'.$value) <span class="text-danger error">{{ $message }}</span>@enderror--}}
{{--                                                                            </div>--}}
{{--                                                                        </div>--}}
{{--                                                                        <div class="row">--}}
{{--                                                                            <div class="col">--}}
{{--                                                                                <button class="btn btn-danger btn-sm" wire:click.prevent="remove({{$key}})">remove</button>--}}
{{--                                                                            </div>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                @endforeach--}}
{{--                                                                <div class="row">--}}
{{--                                                                    <div class="col">--}}
{{--                                                                        <button class="btn text-white btn-info btn-sm" wire:click.prevent="add({{$i}})">Add</button>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            @endif--}}
{{--                                                        </form>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="modal-footer">--}}
{{--                                                    <button wire:click.prevent="update_transfer()"  class="btn btn-primary"><i class="flaticon-cancel-12"></i> Submit</button>--}}
{{--                                                    <button wire:click.prevent="cancel()"  class="btn btn-primary" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>

            </div>
            <livewire:backend.footer />
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->
</div>
@push('scripts')
    <script type="text/javascript">
        window.livewire.on('userStore', () => {
            $('#createInventory').modal('hide');
        });
        window.livewire.on('userUpdate', () => {
            $('#updateModal').modal('hide');
        });
        window.livewire.on('user_selected', () => {
            $('#updateModal_selection').modal('hide');
        });
    </script>
    <script type="text/javascript">
        livewire.on('avatar_preview_updated', NotDestroyTable => {
            $('.table').DataTable( {
                "dom": "<'dt--top-section'<'row'<'col-sm-12 col-md-6 d-flex justify-content-md-start justify-content-center'B><'col-sm-12 col-md-6 d-flex justify-content-md-end justify-content-center mt-md-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'btn btn-sm' },
                        { extend: 'csv', className: 'btn btn-sm' },
                        { extend: 'excel', className: 'btn btn-sm' },
                        { extend: 'print', className: 'btn btn-sm' }
                    ]
                },
                "oLanguage": {
                    "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7
            } ).ajax.reload(originalJsonData,true)
        });
        $('.zero-configuration').DataTable( {
            "dom": "<'dt--top-section'<'row'<'col-sm-12 col-md-6 d-flex justify-content-md-start justify-content-center'B><'col-sm-12 col-md-6 d-flex justify-content-md-end justify-content-center mt-md-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
            buttons: {
                buttons: [
                    { extend: 'copy', className: 'btn btn-sm' },
                    { extend: 'csv', className: 'btn btn-sm' },
                    { extend: 'excel', className: 'btn btn-sm' },
                    { extend: 'print', className: 'btn btn-sm' }
                ]
            },
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        } );
    </script>
@endpush
