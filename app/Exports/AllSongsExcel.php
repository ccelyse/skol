<?php

namespace App\Exports;

use App\Models\SongInfo;
use App\Models\VotingCardTransactions;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AllSongsExcel implements FromCollection
{
//    use Exportable;
//    protected $start_date;
//    protected $end_date;
    protected $export_allSongs;

    use Exportable;
    public function __construct($export_allSongs){
        $this->export_allSongs = $export_allSongs;
//        $this->start_date = $start_date;
//        $this->end_date = $end_date;
    }
    public function collection(){
//        dd($this->export_allSongs);
        return $this->export_allSongs;
//        return SongInfo::whereBetween('created_at',[$this->start_date,$this->end_date]);
//        return MomoTransaction::whereBetween('created_at',[$this->start_date,$this->end_date]);
    }
}
