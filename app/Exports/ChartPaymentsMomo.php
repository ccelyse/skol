<?php

namespace App\Exports;

use App\Models\MomoTransaction;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class ChartPaymentsMomo implements FromQuery
{

    protected $start_date , $end_date;

    use Exportable;
    public function __construct($start_date,$end_date){
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    public function query()
    {
        return MomoTransaction::query()->whereBetween('created_at',[$this->start_date,$this->end_date])->get();
    }
}
