<?php

namespace App\Exports;

use App\Models\MomoTransaction;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ChartPaymentsMomoExport implements FromQuery
{
    use Exportable;
    protected $start_date;
    protected $end_date;

    public function __construct($start_date,$end_date){
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }
    public function query(){
        return MomoTransaction::whereBetween('created_at',[$this->start_date,$this->end_date]);
    }
}
