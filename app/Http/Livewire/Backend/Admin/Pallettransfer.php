<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\Inventory_Distributors;
use App\Models\Inventory_Products;
use App\Models\InventoryLocation;
use App\Models\Pallet_distributor;
use App\Models\Pallet_Management;
use App\Models\PalletManagement_Products;
use App\Models\PalletRepairs;
use App\Models\PalletTransferTracking;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class Pallettransfer extends Component
{
    public $NotDestroyTable;
    public $InventoryLocation;
    public $Inventory_Products;
    public $inventory_update;
    public $distributor_id;
    public $pallet_distributor;
    public $products_ids=[];
    public $InventoryDistributor;
    public $pallet_location_id;
    public $pallet_location_id_transfer;
    public $pallet_product_id;
    public $pallet_management_id;
    public $pallet_product_ids =[];
//    public $Pallet_list;
    public $inputs = [];
    public $selected = [];
    public $i = 1;
    public $updateMode = false;
    public $pallet_name;
    public $pallet_number_from;
    public $pallet_number_to;
    public $searchColumnsBarcode;
    public $pallet_location_id_selected = [];
    public function mount($id,$searchColumnsBarcode=null){
        $this->searchColumnsBarcode = $searchColumnsBarcode;
        $this->pallet_name = $id;
    }
    public function TransferSelected(){
//        dd($this);
        $this->validate([
            'pallet_location_id_transfer' => ['required'],
            'pallet_number_from' => ['required'],
            'pallet_number_to' => ['required'],
        ]);

        $this->pallet_location_id_selected = InventoryLocation::where('inventory_location_name',$this->pallet_name)->value('id');
        $check_pallet_name = InventoryLocation::where('id',$this->pallet_location_id_transfer)->value('inventory_location_name');
        $check_pallet_name_distributor = InventoryLocation::where('id',$this->pallet_location_id_selected)->value('inventory_location_name');
        $pallet_number_from_id = Pallet_Management::where('pallet_barcode', 'LIKE', "%$this->pallet_number_from%")->value('id');
        $pallet_number_to_id = Pallet_Management::where('pallet_barcode', 'LIKE', "%$this->pallet_number_to%")->value('id');
        $getTransferList = Pallet_Management::whereBetween('id', [$pallet_number_from_id, $pallet_number_to_id])->where('pallet_location_id',$this->pallet_location_id_selected)->get();

//        dd($this->pallet_location_id_selected);

        foreach ($getTransferList as $key => $value) {
            $update_location = Pallet_Management::where('id',$value['id'])->update(['pallet_location_id'=>$this->pallet_location_id_transfer]);
        }
        if($check_pallet_name_distributor == "Distributor locations" and $check_pallet_name == "Empties Yard"){
            foreach ($getTransferList as $key => $value) {
                $update_return = Pallet_distributor::where('pallet_management_id',$value['id'])->update(['distributor_return'=>"Yes"]);
            }
        }
        if($check_pallet_name == "Pallet Repairs"){
            foreach ($getTransferList as $key => $value) {
                $pallet_transfer_tracking = new PalletTransferTracking();
                $pallet_transfer_tracking->pallet_management_id = $value['id'];
                $pallet_transfer_tracking->from_location = $this->pallet_location_id_selected;
                $pallet_transfer_tracking->current_location = $this->pallet_location_id_transfer;
                $pallet_transfer_tracking->transfer_user_id = auth()->id();
                $pallet_transfer_tracking->save();

                $check_repair = PalletRepairs::where('pallet_management_id',$value['id'])->value('pallet_number_repairs');

                if(empty($check_repair)){
                    $pallet_repair = new PalletRepairs();
                    $pallet_repair->pallet_management_id = $value['id'];
                    $pallet_repair->repair_user_id = auth()->id();
                    $pallet_repair->pallet_number_repairs = 1;
                    $pallet_repair->save();
                }else{
                    $pallet_repair = new PalletRepairs();
                    $pallet_repair->pallet_management_id = $value['id'];
                    $pallet_repair->repair_user_id = auth()->id();
                    $pallet_repair->pallet_number_repairs = $check_repair + 1;
                    $pallet_repair->save();
                }
            }
        }elseif($check_pallet_name == "Distributor locations"){
            foreach ($getTransferList as $key => $value) {
                $check_pallet_status = Pallet_distributor::where('pallet_management_id',$value['id'])->value('distributor_return');
                if(empty($check_pallet_status) or $check_pallet_status == "Yes"){
                    $pallet_distributor = new Pallet_distributor();
                    $pallet_distributor->pallet_management_id = $value['id'];
                    $pallet_distributor->transfer_user_id = auth()->id();
                    $pallet_distributor->distributor_id = $this->pallet_distributor;
                    $pallet_distributor->save();
                }else{
                    session()->flash('success_delete', 'You can not transfer a pallet when it is not returned');
                }
            }
        }
        else{
            foreach ($getTransferList as $key => $value) {
                $pallet_transfer_tracking = new PalletTransferTracking();
                $pallet_transfer_tracking->pallet_management_id = $value['id'];
                $pallet_transfer_tracking->from_location = $this->pallet_location_id_selected;
                $pallet_transfer_tracking->current_location = $this->pallet_location_id_transfer;
                $pallet_transfer_tracking->transfer_user_id = auth()->id();
                $pallet_transfer_tracking->save();
            }
        }
        $this->resetInputFields();
        $this->emit('user_selected'); // Close model to using to jquery'
        $this->emit('avatar_preview_updated', $this->NotDestroyTable);
        session()->flash('success_delete', 'Successfully transferred pallet');
    }
    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }
    public function remove($i)
    {
        unset($this->inputs[$i]);
    }
    private function resetInputFields(){
        $this->pallet_product_id = '';
        $this->pallet_location_id = '';
        $this->pallet_location_id_transfer = '';
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function render()
    {

//        $this->InventoryLocation = InventoryLocation::where('inventory_location_name','!=',$this->pallet_name)
//            ->where('inventory_location_name','!=',"Distributor locations")->get();
        if($this->pallet_name == "Pallet Manufacturing"){
            $this->InventoryLocation = InventoryLocation::where('inventory_location_name','Empties Yard')->get();
        }elseif ($this->pallet_name == "Empties Yard"){
            $this->InventoryLocation = InventoryLocation::where('inventory_location_name','Packaging')->get();
        }elseif ($this->pallet_name == "Packaging"){
            $this->InventoryLocation = InventoryLocation::whereIn('inventory_location_name',['Finished Goods','Empties Yard','Pallet Repairs'])->get();
        }elseif ($this->pallet_name == "Finished Goods"){
            $this->InventoryLocation = InventoryLocation::whereIn('inventory_location_name',['Distributor locations','Empties Yard'])->get();
        }elseif ($this->pallet_name == "Distributor locations"){
            $this->InventoryLocation = InventoryLocation::where('inventory_location_name','=',"Empties Yard")->get();
        }elseif ($this->pallet_name == "Pallet Repairs"){
            $this->InventoryLocation = InventoryLocation::whereIn('inventory_location_name',['Packaging','Scrapped Pallets'])->get();
        }else{
            $this->InventoryLocation = InventoryLocation::where('inventory_location_name','!=',$this->pallet_name)
                ->where('inventory_location_name','!=',"Distributor locations")->get();
        }
        $this->Inventory_Products = Inventory_Products::all();
        $this->InventoryDistributor = Inventory_Distributors::all();
        $pallet_id = InventoryLocation::where('inventory_location_name',$this->pallet_name)->value('id');

        if($this->searchColumnsBarcode){
            $Pallet_list = Pallet_Management::where('pallet_management.pallet_location_id',$pallet_id)
                ->where('pallet_barcode', 'like', '%'.$this->searchColumnsBarcode.'%')
                ->orderBy('created_at','desc')
                ->join('users', 'users.id', '=', 'pallet_management.pallet_user_id')
                ->join('inventory_locations', 'inventory_locations.id', '=', 'pallet_management.pallet_location_id')
                ->select('pallet_management.*', 'users.name','inventory_locations.inventory_location_name')
                ->get();
            foreach ($Pallet_list as $datas){
                $this->PalletProducts($datas);
                $this->TransferLocations($datas);
                $this->PalletRepairs($datas);
                $this->DistributorName($datas);
                $datas['PalletProducts'] = $this->PalletProducts($datas);
                $datas['TransferLocations'] = $this->TransferLocations($datas);
                $datas['PalletRepairs'] = $this->PalletRepairs($datas);
                $datas['DistributorName'] = $this->DistributorName($datas);
            }
        }else{
            $Pallet_list = Pallet_Management::where('pallet_management.pallet_location_id',$pallet_id)
                ->orderBy('created_at','desc')
                ->join('users', 'users.id', '=', 'pallet_management.pallet_user_id')
                ->join('inventory_locations', 'inventory_locations.id', '=', 'pallet_management.pallet_location_id')
                ->select('pallet_management.*', 'users.name','inventory_locations.inventory_location_name')
                ->get();
            foreach ($Pallet_list as $datas){
                $this->PalletProducts($datas);
                $this->TransferLocations($datas);
                $this->PalletRepairs($datas);
                $this->DistributorName($datas);
                $datas['PalletProducts'] = $this->PalletProducts($datas);
                $datas['TransferLocations'] = $this->TransferLocations($datas);
                $datas['PalletRepairs'] = $this->PalletRepairs($datas);
                $datas['DistributorName'] = $this->DistributorName($datas);
            }
        }

//        $this->Pallet_list = $Pallet_list;
        $this->products_ids = \App\Models\PalletManagement_Products::where('pallet_management_id',$this->pallet_management_id)
            ->join('inventory_products', 'inventory_products.id', '=', 'pallet_management__products.pallet_product_id')
            ->select('inventory_products.product_name','inventory_products.id as product_id')
            ->get();
        $this->emit('avatar_preview_updated', $this->NotDestroyTable);
        return view('livewire.backend.admin.pallettransfer',['Pallet_list' => $Pallet_list])->layout('Layouts.BackendMaster');
    }
    public function DistributorName($datas){
        $distributorName = \App\Models\Pallet_distributor::where('pallet_management_id',$datas->id)
            ->join('inventory_distributors', 'inventory_distributors.id', '=', 'pallet_distributors.distributor_id')
            ->select('inventory_distributors.distributor_name')
            ->latest('pallet_distributors.created_at')->first();
//            ->value('distributor_name');
        return $distributorName;
    }
    public function PalletProducts($datas){
        $palletProducts = \App\Models\PalletManagement_Products::where('pallet_management_id',$datas->id)
            ->join('inventory_products', 'inventory_products.id', '=', 'pallet_management__products.pallet_product_id')
            ->select('inventory_products.product_name','inventory_products.id as product_id')
            ->get();
        return $palletProducts;
    }
    public function TransferLocations($datas){
        $locations = PalletTransferTracking::where('pallet_management_id',$datas->id)
            ->join('inventory_locations as from_l', 'from_l.id', '=', 'pallet_transfer_trackings.from_location')
            ->join('inventory_locations as current_l', 'current_l.id', '=', 'pallet_transfer_trackings.current_location')
            ->join('users', 'users.id', '=', 'pallet_transfer_trackings.transfer_user_id')
            ->select('users.name as transfer_username','from_l.inventory_location_name as from_location','current_l.inventory_location_name as current_location', 'pallet_transfer_trackings.created_at as Transferred_date')
            ->latest('pallet_transfer_trackings.id')
            ->first();
        return $locations;
    }
    public function PalletRepairs($datas){
        $repairs = PalletRepairs::where('pallet_management_id',$datas->id)->count();
        return $repairs;
    }
    public function edit($id)
    {
        $this->updateMode = true;
        $inventory_update = Pallet_Management::where('pallet_management.id',$id)->orderBy('created_at','desc')
            ->join('users', 'users.id', '=', 'pallet_management.pallet_user_id')
            ->join('inventory_locations', 'inventory_locations.id', '=', 'pallet_management.pallet_location_id')
            ->select('pallet_management.*', 'users.name','inventory_locations.inventory_location_name')
            ->get();
        foreach ($inventory_update as $datas){
            $this->PalletProducts($datas);
            $datas['PalletProducts'] = $this->PalletProducts($datas);
            $this->pallet_location_id = $datas->pallet_location_id;
            $this->pallet_management_id = $datas->id;
            $this->products_ids = $datas['PalletProducts'];
        }
    }
    public function update_transfer(){
        $check_pallet_name = InventoryLocation::where('id',$this->pallet_location_id_transfer)->value('inventory_location_name');
        $check_pallet_name_distributor = InventoryLocation::where('id',$this->pallet_location_id)->value('inventory_location_name');
        $update_location = Pallet_Management::where('id',$this->pallet_management_id)->update(['pallet_location_id'=>$this->pallet_location_id_transfer]);
        if($check_pallet_name_distributor == "Distributor locations" and $check_pallet_name == "Empties Yard"){
//            dd($this->pallet_management_id);
            $update_return = Pallet_distributor::where('pallet_management_id',$this->pallet_management_id)->update(['distributor_return'=>"Yes"]);
        }
        if($check_pallet_name == "Pallet Repairs"){
            $pallet_transfer_tracking = new PalletTransferTracking();
            $pallet_transfer_tracking->pallet_management_id = $this->pallet_management_id;
            $pallet_transfer_tracking->from_location = $this->pallet_location_id;
            $pallet_transfer_tracking->current_location = $this->pallet_location_id_transfer;
            $pallet_transfer_tracking->transfer_user_id = auth()->id();
            $pallet_transfer_tracking->save();
            $check_repair = PalletRepairs::where('pallet_management_id',$this->pallet_management_id)->value('pallet_number_repairs');

            if(empty($check_repair)){
                $pallet_repair = new PalletRepairs();
                $pallet_repair->pallet_management_id = $this->pallet_management_id;
                $pallet_repair->repair_user_id = auth()->id();
                $pallet_repair->pallet_number_repairs = 1;
                $pallet_repair->save();
            }else{
                $pallet_repair = new PalletRepairs();
                $pallet_repair->pallet_management_id = $this->pallet_management_id;
                $pallet_repair->repair_user_id = auth()->id();
                $pallet_repair->pallet_number_repairs = $check_repair + 1;
                $pallet_repair->save();
            }
        }elseif($check_pallet_name == "Distributor locations"){
            $check_pallet_status = Pallet_distributor::where('pallet_management_id',$this->pallet_management_id)->value('distributor_return');
            if(empty($check_pallet_status) or $check_pallet_status == "Yes"){
                $pallet_distributor = new Pallet_distributor();
                $pallet_distributor->pallet_management_id = $this->pallet_management_id;
                $pallet_distributor->transfer_user_id = auth()->id();
                $pallet_distributor->distributor_id = $this->pallet_distributor;
                $pallet_distributor->save();
            }else{
                session()->flash('success_delete', 'You can not transfer a pallet when it is not returned');
            }
        }
        else{
            $pallet_transfer_tracking = new PalletTransferTracking();
            $pallet_transfer_tracking->pallet_management_id = $this->pallet_management_id;
            $pallet_transfer_tracking->from_location = $this->pallet_location_id;
            $pallet_transfer_tracking->current_location = $this->pallet_location_id_transfer;
            $pallet_transfer_tracking->transfer_user_id = auth()->id();
            $pallet_transfer_tracking->save();
        }
        if($this->pallet_product_id){
            $validatedDate = $this->validate([
                'pallet_product_id.0' => 'required',
                'pallet_product_id.*' => 'required',
                'pallet_location_id' => 'required',
            ]);
            foreach ($this->pallet_product_id as $key => $value) {
                PalletManagement_Products::create(['pallet_management_id' => $this->pallet_management_id, 'pallet_product_id' => $this->pallet_product_id[$key]]);
            }
        }
        $this->resetInputFields();
        $this->emit('userUpdate'); // Close model to using to jquery
        session()->flash('success_delete', 'Successfully transferred pallet');
    }
    public function PalletRemove($id)
    {
        $product_delete = PalletManagement_Products::where('pallet_product_id', $id)->delete();
        $this->products_ids = \App\Models\PalletManagement_Products::where('pallet_management_id',$this->pallet_management_id)
            ->join('inventory_products', 'inventory_products.id', '=', 'pallet_management__products.pallet_product_id')
            ->select('inventory_products.product_name','inventory_products.id as product_id')
            ->get();
        session()->flash('success_delete', 'Successfully deleted product');
    }
}
