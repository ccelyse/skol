<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\ArtistAccounts;
use App\Models\InventoryLocation;
use App\Models\User;
use Livewire\Component;

class AdminUsers extends Component
{

    public $users;
    public $name;
    public $email;
    public $role;
    public $password;
    public $user_id;
    public $Locations;
    public $updateMode = false;
    public function create(){

        $validatedDate = $this->validate([
            'name' => ['required'],
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'role' => ['required'],
        ]);
//        dd($this);
        $newUser = \App\Models\User::create([
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'password' => bcrypt($this->password)
        ]);
        session()->flash('success','Successfully created a new account');
        $this->resetInputFields();

        $this->emit('userStore'); // Close model to using to jquery
    }
    private function resetInputFields(){
        $this->name = '';
        $this->email = '';
        $this->role = '';
        $this->password = '';
    }
    public function edit($id)
    {
        $this->updateMode = true;
        $user_edit = \App\Models\User::where('id',$id)->first();
//        dd($user_edit);
        $this->user_id = $user_edit->id;
        $this->name = $user_edit->name;
        $this->email = $user_edit->email;
        $this->role = $user_edit->role;

    }
    public function update(){
//        dd($this);
        if ($this->user_id) {
            $update_user = \App\Models\User::find($this->user_id);
            $update_user->name =  $this->name;
            $update_user->email = $this->email;
            $update_user->role = $this->role;
            $update_user->password = bcrypt($this->password);
            $update_user->save();
            $this->updateMode = false;
            $this->resetInputFields();
            $this->emit('userUpdate'); // Close model to using to jquery
            session()->flash('success', 'Successfully Updated user info.');
//            return redirect()->to('/Admin/AdminUsers');

        }
    }
    public function delete($id)
    {
        if($id){
            \App\Models\User::where('id',$id)->delete();
            session()->flash('success', 'Successfully deleted user info.');
//            return redirect()->to('/Admin/AdminUsers');

        }
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function render()
    {
        $this->users = User::orderBy('id','Desc')->get();
        $this->Locations = InventoryLocation::all();
        return view('livewire.backend.admin.admin-users')->layout('Layouts.BackendMaster');
    }
}
