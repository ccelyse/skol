<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\Inventory_Distributors;
use Livewire\Component;

class InventoryDistributors extends Component
{
    public $InventoryDistributor;
    public $distributor_id;
    public $distributor_name;
    public function render()
    {
        $this->InventoryDistributor = Inventory_Distributors::all();
        return view('livewire.backend.admin.inventory-distributors')->layout('Layouts.BackendMaster');
    }
    public function create(){
        $validatedDate = $this->validate([
            'distributor_name' => ['required'],
        ]);
        $new= new \App\Models\Inventory_Distributors();
        $new->distributor_name = $this->distributor_name;
        $new->save();
        session()->flash('success', 'You have successfully added a new distributor');
        $this->resetInputFields();
        $this->emit('userStore'); // Close model to using to jquery
    }
    private function resetInputFields(){
        $this->distributor_name = '';
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function edit($id)
    {
        $this->updateMode = true;
        $inventory_update = \App\Models\Inventory_Distributors::where('id',$id)->first();
//        dd($user_edit);
        $this->distributor_id = $inventory_update->id;
        $this->distributor_name = $inventory_update->distributor_name;

    }
    public function update(){
//        dd($this);
        if ($this->distributor_id) {
            $update_user = \App\Models\Inventory_Distributors::find($this->distributor_id);
            $update_user->distributor_name =  $this->distributor_name;
            $update_user->save();
            $this->updateMode = false;
            $this->resetInputFields();
            $this->emit('userUpdate'); // Close model to using to jquery
            session()->flash('success', 'Successfully Updated distributor info.');
        }
    }
    public function delete($id)
    {
        if($id){
            $distributor = Inventory_Distributors::where('id',$id)->delete();
            session()->flash('success', 'Successfully deleted distributor.');
        }
    }
}
