<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\Inventory_Products;
use App\Models\InventoryLocation;
use Livewire\Component;

class InventoryProducts extends Component
{
    public $InventoryProducts;
    public $product_name;
    public $inventory_id;
    public $updateMode = false;

    public function render()
    {
        $this->InventoryProducts = Inventory_Products::all();
        return view('livewire.backend.admin.inventory-products')->layout('Layouts.BackendMaster');
    }
    public function create(){
        $validatedDate = $this->validate([
            'product_name' => ['required'],
        ]);
        $new= new \App\Models\Inventory_Products();
        $new->product_name=$this->product_name;
        $new->save();
        session()->flash('success', 'You have successfully added a new product');
        $this->resetInputFields();
        $this->emit('userStore'); // Close model to using to jquery
    }
    private function resetInputFields(){
        $this->product_name = '';
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function edit($id)
    {
        $this->updateMode = true;
        $inventory_update = \App\Models\Inventory_Products::where('id',$id)->first();
//        dd($user_edit);
        $this->inventory_id = $inventory_update->id;
        $this->product_name = $inventory_update->product_name;

    }
    public function update(){
//        dd($this);
        if ($this->inventory_id) {
            $update_user = \App\Models\Inventory_Products::find($this->inventory_id);
            $update_user->product_name =  $this->product_name;
            $update_user->save();
            $this->updateMode = false;
            $this->resetInputFields();
            $this->emit('userUpdate'); // Close model to using to jquery
            session()->flash('success', 'Successfully Updated product info.');
        }
    }
    public function delete($id)
    {
        if($id){
            $product = Inventory_Products::where('id',$id)->delete();
            session()->flash('success', 'Successfully deleted product.');
        }
    }
}
