<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\Inventory_Distributors;
use App\Models\Inventory_Trucks;
use Livewire\Component;

class InventoryTrucks extends Component
{
    public $Inventory_Trucks;
    public $Inventory_Distributors;
    public $truck_number_plate;
    public $distributor_id;
    public $truck_id;
    public function render()
    {
        $this->Inventory_Trucks = Inventory_Trucks::join('inventory_distributors', 'inventory_distributors.id', '=', 'inventory_trucks.distributor_id')
            ->select('inventory_trucks.*', 'inventory_distributors.distributor_name')->get();
        $this->Inventory_Distributors = Inventory_Distributors::all();
        return view('livewire.backend.admin.inventory-trucks')->layout('Layouts.BackendMaster');
    }
    public function create(){
        $validatedDate = $this->validate([
            'distributor_id' => ['required'],
            'truck_number_plate' => ['required'],
        ]);
        $new= new \App\Models\Inventory_Trucks();
        $new->distributor_id = $this->distributor_id;
        $new->truck_number_plate = $this->truck_number_plate;
        $new->save();
        session()->flash('success', 'You have successfully added a new truck');
        $this->resetInputFields();
        $this->emit('userStore'); // Close model to using to jquery
    }
    private function resetInputFields(){
        $this->distributor_id = '';
        $this->truck_number_plate = '';
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function edit($id)
    {
        $this->updateMode = true;
        $truck_update = \App\Models\Inventory_Trucks::where('id',$id)->first();
//        dd($user_edit);
        $this->truck_id = $truck_update->id;
        $this->distributor_id = $truck_update->truck_number_plate;
        $this->truck_number_plate = $truck_update->truck_number_plate;

    }
    public function update(){
//        dd($this);
        if ($this->truck_id) {
            $update_truck = \App\Models\Inventory_Trucks::find($this->truck_id);
            $update_truck->distributor_id =  $this->distributor_id;
            $update_truck->truck_number_plate =  $this->truck_number_plate;
            $update_truck->save();
            $this->updateMode = false;
            $this->resetInputFields();
            $this->emit('userUpdate'); // Close model to using to jquery
            session()->flash('success', 'Successfully Updated truck info.');
        }
    }
    public function delete($id)
    {
        if($id){
            $truck = Inventory_Trucks::where('id',$id)->delete();
            session()->flash('success', 'Successfully deleted truck.');
        }
    }
}
