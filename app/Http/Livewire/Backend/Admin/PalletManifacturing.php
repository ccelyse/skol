<?php

namespace App\Http\Livewire\Backend\Admin;

use Livewire\Component;

class PalletManifacturing extends Component
{
    public function render()
    {
        return view('livewire.backend.admin.pallet-manifacturing');
    }
}
