<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\Inventory_Distributors;
use App\Models\Inventory_Products;
use App\Models\InventoryLocation;
use App\Models\Pallet_distributor;
use App\Models\Pallet_Management;
use App\Models\PalletManagement_Products;
use App\Models\PalletRepairs;
use App\Models\PalletTransferTracking;
use App\Models\TransferAllList;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class ScanPallet extends Component
{
    public $InventoryLocation;
    public $Inventory_Products;
    public $inventory_update;
    public $distributor_id;
    public $pallet_distributor;
    public $products_ids=[];
    public $InventoryDistributor;
    public $pallet_location_id;
    public $pallet_location_id_transfer;
    public $pallet_product_id;
    public $pallet_management_id;
    public $pallet_product_ids =[];
    public $Pallet_list;
    public $inputs = [];
    public $i = 1;
    public $updateMode = false;
    public $pallet_name;
    public $pallet_name_barcode;
    public $pallet_title = [];
    public function mount($id){
        $this->pallet_name_barcode = $id;
    }

    public function render()
    {
        $get_pallete_id = Pallet_Management::where('pallet_barcode',$this->pallet_name_barcode)->value('pallet_location_id');
        if(empty($get_pallete_id)){
            $this->pallet_title = "This pallete can not be found in database, scan again";
        }
        $this->pallet_name = InventoryLocation::where('id',$get_pallete_id)->value('inventory_location_name');
        $Pallet_list = Pallet_Management::where('pallet_management.pallet_barcode',$this->pallet_name_barcode)
            ->orderBy('created_at','desc')
            ->join('users', 'users.id', '=', 'pallet_management.pallet_user_id')
            ->join('inventory_locations', 'inventory_locations.id', '=', 'pallet_management.pallet_location_id')
            ->select('pallet_management.*', 'users.name','inventory_locations.inventory_location_name')
            ->get();
        foreach ($Pallet_list as $datas){
            $this->TransferLocations($datas);
            $this->PalletRepairs($datas);
            $datas['TransferLocations'] = $this->TransferLocations($datas);
            $datas['PalletRepairs'] = $this->PalletRepairs($datas);
        }
        $this->Pallet_list = $Pallet_list;
        return view('livewire.backend.admin.scan-pallet')->layout('Layouts.BackendMaster');
    }

    public function TransferLocations($datas){
        $locations = PalletTransferTracking::where('pallet_management_id',$datas->id)
            ->join('inventory_locations as from_l', 'from_l.id', '=', 'pallet_transfer_trackings.from_location')
            ->join('inventory_locations as current_l', 'current_l.id', '=', 'pallet_transfer_trackings.current_location')
            ->join('users', 'users.id', '=', 'pallet_transfer_trackings.transfer_user_id')
            ->select('users.name as transfer_username','from_l.inventory_location_name as from_location','current_l.inventory_location_name as current_location', 'pallet_transfer_trackings.created_at as Transferred_date')
            ->latest('pallet_transfer_trackings.id')
            ->first();
        return $locations;
    }
    public function PalletRepairs($datas){
        $repairs = PalletRepairs::where('pallet_management_id',$datas->id)->count();
        return $repairs;
    }
    public function addList($id){
//        dd($id);
        $add_transfer = new TransferAllList();
        $add_transfer->pallet_management_id = $id;
        $add_transfer->transfer_user_id = auth()->id();
        $add_transfer->save();
        return redirect()->to('/Admin/PendingTransfers');
    }
}
