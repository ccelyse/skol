<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\InventoryLocation;
use Livewire\Component;

class InvetoryLocation extends Component
{
    public $inventory_location_name;
    public $InventoryList;
    public $inventory_id;
    public $updateMode = false;
    public function render()
    {
        $this->InventoryList = InventoryLocation::all();
        return view('livewire.backend.admin.invetory-location')->layout('Layouts.BackendMaster');
    }
    public function create(){
        $validatedDate = $this->validate([
            'inventory_location_name' => ['required'],
        ]);
        $new= new \App\Models\InventoryLocation();
        $new->inventory_location_name=$this->inventory_location_name;
        $new->save();
        session()->flash('success', 'You have successfully added a new pallet');
        $this->resetInputFields();
        $this->emit('userStore'); // Close model to using to jquery
    }
    private function resetInputFields(){
        $this->inventory_location_name = '';
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function edit($id)
    {
        $this->updateMode = true;
        $inventory_update = \App\Models\InventoryLocation::where('id',$id)->first();
//        dd($user_edit);
        $this->inventory_id = $inventory_update->id;
        $this->inventory_location_name = $inventory_update->inventory_location_name;

    }
    public function update(){
//        dd($this);
        if ($this->inventory_id) {
            $update_user = \App\Models\InventoryLocation::find($this->inventory_id);
            $update_user->inventory_location_name =  $this->inventory_location_name;
            $update_user->save();
            $this->updateMode = false;
            $this->resetInputFields();
            $this->emit('userUpdate'); // Close model to using to jquery
            session()->flash('success', 'Successfully Updated Pallet info.');
        }
    }
    public function delete($id)
    {
        if($id){
            $location = InventoryLocation::where('id',$id)->delete();
            session()->flash('success', 'Successfully deleted Pallet.');
        }
    }
}
