<?php

namespace App\Http\Livewire\Backend\Admin;

use App\Models\Inventory_Distributors;
use App\Models\Inventory_Products;
use App\Models\InventoryLocation;
use App\Models\Pallet_Management;
use App\Models\PalletManagement_Products;
use Carbon\Carbon;
use Livewire\Component;

class PalletManagement extends Component
{
    public $NotDestroyTable;
    public $InventoryLocation;
    public $Inventory_Products;
    public $InventoryDistributor;
    public $pallet_location_id;
    public $pallet_product_id;
    public $Pallet_list;
    public $pallet_quality;
    public $inputs = [];
    public $i = 1;
    public function render()
    {
        $this->InventoryLocation = InventoryLocation::where('inventory_location_name','Pallet Manufacturing')->get();
        $this->Inventory_Products = Inventory_Products::all();
        $this->InventoryDistributor = Inventory_Distributors::all();
        $Pallet_list = Pallet_Management::orderBy('id','desc')
            ->join('users', 'users.id', '=', 'pallet_management.pallet_user_id')
            ->join('inventory_locations', 'inventory_locations.id', '=', 'pallet_management.pallet_location_id')
            ->select('pallet_management.*', 'users.name','inventory_locations.inventory_location_name')
            ->get();
        foreach ($Pallet_list as $datas){
            $this->PalletProducts($datas);
            $datas['PalletProducts'] = $this->PalletProducts($datas);
        }
        $this->Pallet_list = $Pallet_list;
        $this->emit('avatar_preview_updated', $this->NotDestroyTable);
        return view('livewire.backend.admin.pallet-management')->layout('Layouts.BackendMaster');

    }
    public function PalletProducts($datas){
        $palletProducts = \App\Models\PalletManagement_Products::where('pallet_management_id',$datas->id)
            ->join('inventory_products', 'inventory_products.id', '=', 'pallet_management__products.pallet_product_id')
            ->select('inventory_products.product_name')
            ->get();
        return $palletProducts;
    }
    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }
    public function remove($i)
    {
        unset($this->inputs[$i]);
    }
    public function create(){
//        dd($this);
        $validatedDate = $this->validate([
//            'pallet_product_id.0' => 'required',
//            'pallet_product_id.*' => 'required',
            'pallet_location_id' => 'required',
            'pallet_quality' => 'required',
        ]);

        $currentMonth = Carbon::now()->format('m');
        $currentYear = Carbon::now()->format('y');
        $pallet_type = "SKL";
        $lastid = Pallet_Management::all()->last();

        if($lastid == null) {
            for($i = 0; $i < $this->pallet_quality; $i++) {
                $barcode_number = $currentYear . $currentMonth . $pallet_type . "0";
                $new = new \App\Models\Pallet_Management();
                $new->pallet_location_id = $this->pallet_location_id;
                $new->pallet_barcode = $barcode_number;
                $new->pallet_user_id = auth()->id();
                $new->save();
                $last_id_pallet = $new->id;
            }
            if($this->pallet_product_id){
                foreach ($this->pallet_product_id as $key => $value) {
                    PalletManagement_Products::create(['pallet_management_id' => $last_id_pallet, 'pallet_product_id' => $this->pallet_product_id[$key]]);
                }
            }
        }else{
            for($i = 0; $i < $this->pallet_quality; $i++) {
                $lastid = Pallet_Management::all()->last()->id;
                $barcode_number = $currentYear.$currentMonth.$pallet_type.$lastid;
                $new= new \App\Models\Pallet_Management();
                $new->pallet_location_id = $this->pallet_location_id;
                $new->pallet_barcode = $barcode_number;
                $new->pallet_user_id = auth()->id();
                $new->save();
            }
//            if($this->pallet_product_id){
//                for ($x = 0; $x <= 10; $x++) {
//                    echo "The number is: $x <br>";
//                }
////                foreach ($this->pallet_product_id as $key => $value) {
////                    PalletManagement_Products::create(['pallet_management_id' => $last_id_pallet, 'pallet_product_id' => $this->pallet_product_id[$key]]);
////                }
//            }
        }
        $this->inputs = [];
        session()->flash('success', 'You have successfully added a new pallet');
        $this->emit('userStore'); // Close model to using to jquery
        $this->resetInputFields();
        $this->emit('avatar_preview_updated', $this->NotDestroyTable);
    }
    private function resetInputFields(){
        $this->pallet_product_id = '';
        $this->pallet_location_id = '';
        $this->pallet_quality = '';
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function edit($id)
    {
        $this->updateMode = true;
        $inventory_update = \App\Models\Inventory_Distributors::where('id',$id)->first();
//        dd($user_edit);
        $this->distributor_id = $inventory_update->id;
        $this->distributor_name = $inventory_update->distributor_name;

    }
    public function update(){
//        dd($this);
        if ($this->distributor_id) {
            $update_user = \App\Models\Inventory_Distributors::find($this->distributor_id);
            $update_user->distributor_name =  $this->distributor_name;
            $update_user->save();
            $this->updateMode = false;
            $this->resetInputFields();
            $this->emit('userUpdate'); // Close model to using to jquery
            session()->flash('success', 'Successfully Updated distributor info.');
        }
    }
    public function delete($id)
    {
        if($id){
            $product_delete = PalletManagement_Products::where('pallet_management_id', $id)->delete();
            $pallet_delete = Pallet_Management::where('id',$id)->delete();
            session()->flash('success', 'Successfully deleted Pallet.');
        }
    }
}
