<?php

namespace App\Http\Livewire\Backend\User;

use App\Models\InventoryLocation;
use App\Models\Pallet_Management;
use App\Models\PalletRepairs;
use App\Models\PalletTransferTracking;
use Livewire\Component;

class Dashboard extends Component
{
    public $pallet_number;
    public $Locations;
    public $Pallet_list;
    public $pallet_name = [];
    public function render()
    {
        $this->pallet_number = Pallet_Management::count();
        $Locations = InventoryLocation::all();
        foreach ($Locations as $datas){
            $this->PalletNumbers($datas);
            $datas['PalletNumbers'] = $this->PalletNumbers($datas);
        }
        $pallet_tracking = PalletTransferTracking::orderBy('updated_at','desc')->get();
        foreach ($pallet_tracking as $data){
            $ids = $data->pallet_management_id;
        }
        $ids= array();
        foreach($pallet_tracking as $data){
            array_push($ids, $data->pallet_management_id);
        }
        $Pallet_list = Pallet_Management::whereIn('pallet_management.id',$ids)
            ->join('users', 'users.id', '=', 'pallet_management.pallet_user_id')
            ->join('inventory_locations', 'inventory_locations.id', '=', 'pallet_management.pallet_location_id')
            ->select('pallet_management.*', 'users.name','inventory_locations.inventory_location_name')
            ->get();
        foreach ($Pallet_list as $datas){
            $this->PalletProducts($datas);
            $this->TransferLocations($datas);
            $this->PalletRepairs($datas);
            $datas['PalletProducts'] = $this->PalletProducts($datas);
            $datas['TransferLocations'] = $this->TransferLocations($datas);
            $datas['PalletRepairs'] = $this->PalletRepairs($datas);
        }
        $this->Pallet_list = $Pallet_list;
        $this->Locations = $Locations;
        return view('livewire.backend.user.dashboard')->layout('Layouts.BackendMaster');
    }
    public function PalletNumbers($datas){
        $pallet_numbers = Pallet_Management::where('pallet_location_id',$datas->id)->count();
        return $pallet_numbers;
    }
    public function PalletProducts($datas){
        $palletProducts = \App\Models\PalletManagement_Products::where('pallet_management_id',$datas->id)
            ->join('inventory_products', 'inventory_products.id', '=', 'pallet_management__products.pallet_product_id')
            ->select('inventory_products.product_name','inventory_products.id as product_id')
            ->get();
        return $palletProducts;
    }
    public function TransferLocations($datas){
        $locations = PalletTransferTracking::where('pallet_management_id',$datas->id)
            ->join('inventory_locations as from_l', 'from_l.id', '=', 'pallet_transfer_trackings.from_location')
            ->join('inventory_locations as current_l', 'current_l.id', '=', 'pallet_transfer_trackings.current_location')
            ->join('users', 'users.id', '=', 'pallet_transfer_trackings.transfer_user_id')
            ->select('users.name as transfer_username','from_l.inventory_location_name as from_location','current_l.inventory_location_name as current_location', 'pallet_transfer_trackings.created_at as Transferred_date')
            ->latest('pallet_transfer_trackings.id')
            ->first();
        return $locations;
    }
    public function PalletRepairs($datas){
        $repairs = PalletRepairs::where('pallet_management_id',$datas->id)->value('pallet_number_repairs');
        return $repairs;
    }
}
