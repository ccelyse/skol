<?php

namespace App\Http\Livewire\Backend\User;

use App\Models\User;
use Livewire\Component;

class Account extends Component
{
    public $users;
    public $name;
    public $email;
    public $role;
    public $password;
    public $user_id;
    public $updateMode = false;
    private function resetInputFields(){
        $this->name = '';
        $this->email = '';
        $this->role = '';
        $this->password = '';
    }
    public function edit($id)
    {
        $this->updateMode = true;
        $user_edit = \App\Models\User::where('id',$id)->first();
//        dd($user_edit);
        $this->user_id = $user_edit->id;
        $this->name = $user_edit->name;
        $this->email = $user_edit->email;
        $this->role = $user_edit->role;

    }
    public function update(){
//        dd($this);
        if ($this->user_id) {
            $update_user = \App\Models\User::find($this->user_id);
            $update_user->name =  $this->name;
            $update_user->email = $this->email;
            $update_user->role = $this->role;
            $update_user->password = $this->password;
            $update_user->save();
            $this->updateMode = false;
            $this->resetInputFields();
            $this->emit('userUpdate'); // Close model to using to jquery
            session()->flash('success', 'Successfully Updated user info.');
        }
    }
    public function delete($id)
    {
        if($id){
            $user_email = User::where('id',$id)->value('email');
            $artist_profile = ArtistAccounts::where('artist_email',$user_email)->delete();
            \App\Models\User::where('id',$id)->delete();
            session()->flash('success', 'Successfully deleted user info.');
//            return redirect()->to('/Admin/AdminUsers');

        }
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }
    public function render()
    {
        $this->users = User::where('id',auth()->id())->get();
        return view('livewire.backend.user.account')->layout('Layouts.BackendMaster');
    }
}
