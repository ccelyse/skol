<?php

namespace App\Http\Livewire\Backend;

use App\Models\ArtistAccounts;
use App\Models\InventoryLocation;
use App\Models\User;
use Livewire\Component;

class SecMenu extends Component
{
    public $avatar;
    public $pallet_menu;
    public function render()
    {
        $profile_pic=User::where('email',auth()->user()->email)->value('name');
        $this->avatar=$profile_pic;
        $this->pallet_menu = InventoryLocation::all();
        return view('livewire.backend.sec-menu');
    }
}
