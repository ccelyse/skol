<?php

namespace App\Http\Livewire\Backend;

use App\Models\User;
use Livewire\Component;
use App\Models\ArtistAccounts;

class Header extends Component
{
    public $avatar;
    public function render()
    {
        $profile_pic=User::where('email',auth()->user()->email)->value('name');
        $this->avatar=$profile_pic;
        return view('livewire.backend.header');
    }
}
