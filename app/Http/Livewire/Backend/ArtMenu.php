<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class ArtMenu extends Component
{
    public function render()
    {
        return view('livewire.backend.art-menu');
    }
}
