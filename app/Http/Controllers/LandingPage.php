<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingPage extends Controller
{
    //
    public function loadHomePage(){
    	return view('home');
    }
}
