<?php

namespace App\Http\Controllers;

use App\Facades\Cart as CartFacade;
use App\Models\Country;
use App\Models\Pallet_Management;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Picqer;
use PDF;
class BackendController extends Controller
{
    public function index()
    {
        $role = Auth::user()->role;
        $checkrole = explode(',', $role);
        if (in_array('Admin', $checkrole)) {
            return redirect('/Admin/Dashboard');
        }
        elseif (in_array('User', $checkrole) or in_array('Pallet Manufacturing', $checkrole) or in_array('Empties Yard', $checkrole) or
            in_array('Packaging', $checkrole) or in_array('Pallet Repairs', $checkrole) or in_array('Scrapped Pallets', $checkrole) or
            in_array('Finished Goods', $checkrole) or in_array('Distributor locations', $checkrole)) {
            return redirect('/User/Dashboard');
        }else{
            Auth::logout();
            Session::flush();
            return redirect('/login');
        }
    }
    public function login(){
        return view('auth.login');
    }
    public function PrintPallet(Request $request){
        $barcode_number = Pallet_Management::where('id', $request['id'])->value('pallet_barcode');
        $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
        $barcode = $generator->getBarcode($barcode_number, $generator::TYPE_CODE_128);
        $name = $barcode_number. ".pdf";
        view()->share('PrintPallet',$barcode_number);
        $pdf = PDF::loadView('PrintPallet', compact('barcode_number',))->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        return $pdf->download($name);
//        return view('PrintPallet',compact('barcode_number','barcode'));
    }
    public function ScanPallet(){
        return view('Scan-pallet');
    }
}
