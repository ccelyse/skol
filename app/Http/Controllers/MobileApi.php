<?php

namespace App\Http\Controllers;

use App\Models\BookGenres;
use App\Models\Books;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
//use Illuminate\Validation\Validator;
use \Validator;

class MobileApi extends Controller
{
    public function register(Request $request){
        $validator = Validator::make($request->all(),[
           'first_name' => 'required',
           'last_name' => 'required',
           'age' => 'required',
           'gender' => 'required',
           'country_id' => 'required',
           'phone_number' => 'required',
           'password' => 'required',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
        ]);
        if ($validator->fails()){
            return response()->json([
                'status_code'=> 400,
                'message'=> "Bad request",
            ]);
        }
        $names = $request['first_name'].' '.$request['last_name'];
        $newUser = new User();
        $newUser->name = $names;
        $newUser->age = $request->age;
        $newUser->gender = $request->gender;
        $newUser->country_id = $request->country_id;
        $newUser->phone_number = $request->phone_number;
        $newUser->email = $request->email;
        $newUser->phone_number = $request->phone_number;
        $newUser->status = "Active";
        $newUser->role = "Reader";
        $newUser->password = Hash::make($this->password);

        if ($request->hasFile('image_name')) {
            $destinationPath = public_path('/storage/UserImages');
            $files = $request->file('image_name'); // will get all files
//            dd($files);
            $file_name = rand() . '.' . $files->getClientOriginalExtension(); //Get file original name
            $files->move($destinationPath , $file_name); // move files to destination folder
            $newUser->image_name = $file_name;
        }

        return response()->json([
            'status_code'=> 200,
            'message'=> "User successfully registered",
        ]);
    }
    public function UserLogin(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json([
                'status_code'=> 400,
                'message'=> "Bad request",
            ]);
        }
        $credentials = request(['email','password']);

        $user = User::where('email',$request['email'])->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

//        if(Auth::attempt($credentials)){
//            return response()->json([
//                'status_code'=> 500,
//                'message'=> "Unauthorised",
//            ]);
//        }
//
        $tokenResult = $user->createToken('authToken')->plainTextToken;

        return response()->json([
            'status_code'=> 200,
            'message'=> "authorised",
            'token' => $tokenResult,
            'user_info' => $user
        ]);

    }
    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'status_code'=> 200,
            'message'=> "Successfully logged out",
        ]);
    }
    public function FeaturedCategories(){
        $categories  = BookGenres::all();
        if(0 == count($categories)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $categories->toArray();
        }
    }
    public function GenreBooks(Request $request){
        $books = Books::where('book_category',$request['id'])->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->get();
        if(0 == count($books)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $books->toArray();
        }
    }
    public function NewRelease(){
        $book = Books::join('users', 'users.id', '=', 'books.user_id')
            ->select('books.*', 'users.name')
            ->limit(6)
            ->orderBy('id','desc')
            ->get();
        $book->makeHidden(['book_file_name']);
        if(0 == count($book)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $book->toArray();
        }
    }
    public function PopularBooks(){
        $book = Books::join('users', 'users.id', '=', 'books.user_id')
            ->select('books.*', 'users.name')
            ->limit(8)
            ->orderBy('id','desc')
            ->get();
        $book->makeHidden(['book_file_name']);
        if(0 == count($book)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $book->toArray();
        }
    }
    public function BookMore(Request $request){
        $books = Books::where('books.id',$request['id'])->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->get();
        $books->makeHidden(['book_file_name']);
        if(0 == count($books)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $books->toArray();
        }
    }
    public function AuthorProfile(Request $request){
        $author = User::where('users.id',$request['id'])->join('author_details', 'author_details.user_id', '=', 'users.id')
            ->join('apps_countries', 'apps_countries.id', '=', 'users.country_id')
            ->select('author_details.*', 'users.name','users.email','users.country_id','users.image_name','apps_countries.country_name')
            ->get();
        $books = Books::where('user_id',$request['id'])
            ->join('users', 'users.id', '=', 'books.user_id')
            ->select('books.*', 'users.name')
            ->limit(4)
            ->orderBy('id','desc')
            ->get();
        $books->makeHidden(['book_file_name']);

        $author_books = Books::where('books.user_id',$request['id'])
            ->join('users', 'users.id', '=', 'books.user_id')
            ->join('book_genres', 'book_genres.id', '=', 'books.book_category')
            ->select('books.*', 'users.name','book_genres.genre_name')
            ->orderBy('id','desc')
            ->get();
        $author_books->makeHidden(['book_file_name']);

        if(0 == count($author)){
            return response()->json([
                'response_message' => "Author can not be found",
                'response_status' =>400
            ]);
        }else{
            return response()->json([
                'Author_info' => $author,
                'Author_books' =>$author_books
            ]);
        }
    }
}
