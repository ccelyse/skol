<?php

namespace App\Http\Controllers;

use App\Models\BookGenres;
use App\Models\CompetitionCardTransactions;
use App\Models\CompetitionTransactions;
use App\Models\CompVotes;
use App\Models\Contestants;
use App\Models\MomoTransaction;
use App\Models\SongInfo;
use App\Models\SongVotes;
use App\Models\SpennCompetitionTransactions;
use App\Models\SpennTransactions;
use App\Models\Votes;
use App\Models\content_Titles;
use App\Models\VotingCardTransactions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FrontendController extends Controller
{

    public function UpdatePaymentSpenn(Request $request){

        if ($request["RequestGuid"]){
            $curl = curl_init();
            //Generating Payment Gateway
            curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://uat-idsrv.spenn.com/token',
                CURLOPT_URL => 'https://idsrv.spenn.com/token',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS => 'grant_type=api_key&api_key=R1b4GGTZozgCiA4NS3N4uF6tL8z8zDSbHzFIOMe5w2tR1ygGNeXbYDfEpY3jHszxgORoQss4oO8%3D&client_id=SpennBusinessApiKey&client_secret=1234&audience=SpennBusiness',
                CURLOPT_POSTFIELDS => 'grant_type=api_key&api_key=s7Uhe%2FzZAdm1gSlR4st2MjnZKUU5dnL1XVqsxw37JCcowCQmDDlclc246qm%2Bf7olV0J3xIbwo64%3D&client_id=SpennBusinessApiKey&client_secret=1234&audience=SpennBusiness',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded',
                    'Cookie: ARRAffinity=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e; ARRAffinitySameSite=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e'
                ),
            ));
            $response = curl_exec($curl);
            $response_decode = json_decode($response);
            $token = $response_decode->access_token;
            $RequestGuid = $request["RequestGuid"];
            $url = "https://businessapi.spenn.com/api/Partner/transaction/request/$RequestGuid/status";

            if ($token) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        "Authorization:Bearer $token",
                        'Cookie: ARRAffinity=897f733e0a4bb2d970e3093451d855969946ff77291544916a2dec8450ff463f; ARRAffinitySameSite=897f733e0a4bb2d970e3093451d855969946ff77291544916a2dec8450ff463f'
                    ),
                ));

                $response_v1 = curl_exec($curl);
                $json_response_api = json_decode($response_v1);

                if (!curl_errno($curl)) {
                    switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                        case 200:  # OK
                            if($json_response_api->requestStatus == "Approved"){
                                $firstday = new Carbon('first day of last month');
                                $lastday = new Carbon('last day of last month');
                                $lastday_ = $lastday->subDays(7);
                                $firstdaydate = $firstday->toDateString();
                                $lastdaydate = $lastday_->toDateString();

                                $get_voter_info = SpennTransactions::where('transaction_id', $request["RequestGuid"])->first();
                                $get_vote_id = SpennTransactions::where('transaction_id',$request["RequestGuid"])->value('voter_id');
                                $vote = new Votes();
                                $vote->starting_date = $firstdaydate;
                                $vote->ending_date = $lastdaydate;
                                $vote->voterphonenumber = $get_voter_info->phonenumber;
                                $vote->vote = ($get_voter_info->amount) / 25;
                                $vote->voter_status = "SUCCESSFUL";
                                $vote->voter_artist = $get_voter_info->voter_artist;
                                $vote->voter_artist_song = $get_voter_info->voter_artist_song;
                                $vote->song_id = $get_voter_info->song_id;
                                $vote->save();
                                $last_id = $vote->id;

                                $votepivot = new SongVotes();
                                $votepivot->song_id = $get_voter_info->song_id;
                                $votepivot->vote_id = $last_id;
                                $votepivot->save();

                                $update_speen = SpennTransactions::where('transaction_id',$request["RequestGuid"])->update(['status'=>'SUCCESSFUL','voter_id' => $last_id]);
                                return response()->json([
                                    'message' => "Payment successfully updated",
                                    'response_status' => 'success'
                                ]);

                            }else{
                                $update_speen = SpennTransactions::where('transaction_id', $request["RequestGuid"])->update(['status' => "Pending"]);
                                return response()->json([
                                    'message' => "Payment status not updated",
                                    'response_status' => 'failed'
                                ]);
                            }
                            break;
                        default:
                            return response()->json([
                                'message' => "Transactions Id doesn't exist",
                                'response_status' => 'failed'
                            ]);
                    }
                }

            }
        }

    }
    public function UpdatePaymentSpennCompetition(Request $request){
//        Log::info($request->all());
//        dd($request->all());
        if ($request["RequestGuid"]){
            $curl = curl_init();
            //Generating Payment Gateway
            curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://uat-idsrv.spenn.com/token',
                CURLOPT_URL => 'https://idsrv.spenn.com/token',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS => 'grant_type=api_key&api_key=R1b4GGTZozgCiA4NS3N4uF6tL8z8zDSbHzFIOMe5w2tR1ygGNeXbYDfEpY3jHszxgORoQss4oO8%3D&client_id=SpennBusinessApiKey&client_secret=1234&audience=SpennBusiness',
                CURLOPT_POSTFIELDS => 'grant_type=api_key&api_key=s7Uhe%2FzZAdm1gSlR4st2MjnZKUU5dnL1XVqsxw37JCcowCQmDDlclc246qm%2Bf7olV0J3xIbwo64%3D&client_id=SpennBusinessApiKey&client_secret=1234&audience=SpennBusiness',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded',
                    'Cookie: ARRAffinity=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e; ARRAffinitySameSite=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e'
                ),
            ));
            $response = curl_exec($curl);
            $response_decode = json_decode($response);
            $token = $response_decode->access_token;
            $RequestGuid = $request["RequestGuid"];
            $url = "https://businessapi.spenn.com/api/Partner/transaction/request/$RequestGuid/status";

            if ($token) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        "Authorization:Bearer $token",
                        'Cookie: ARRAffinity=897f733e0a4bb2d970e3093451d855969946ff77291544916a2dec8450ff463f; ARRAffinitySameSite=897f733e0a4bb2d970e3093451d855969946ff77291544916a2dec8450ff463f'
                    ),
                ));

                $response_v1 = curl_exec($curl);
                $json_response_api = json_decode($response_v1);

                if (!curl_errno($curl)) {
                    switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                        case 200:  # OK
                            if($json_response_api->requestStatus == "Approved"){
                                $get_voter_info = SpennCompetitionTransactions::where('transaction_id', $request["RequestGuid"])->first();
                                $competition_id = Contestants::where('id', $get_voter_info->contestant_id)->value('competition_id');
                                $addvote = new CompVotes();
                                $addvote->vote = ($get_voter_info->amount) / 50;
                                $addvote->names = $get_voter_info->voter_names;
                                $addvote->competition_id = $competition_id;
                                $addvote->contestant_id = $get_voter_info->contestant_id;
                                $addvote->payment_from = "Spenn";
                                $addvote->payment_tx_ref = $get_voter_info->transaction_id;
                                $addvote->status = "SUCCESSFUL";
                                $addvote->save();
                                $last_id = $addvote->id;
                                $update_speen = SpennCompetitionTransactions::where('transaction_id', $request["RequestGuid"])->update(['status' => 'SUCCESSFUL', 'voter_id' => $last_id]);
                                return response()->json([
                                    'message' => "Payment successfully updated",
                                    'response_status' => 'success'
                                ]);
                            }else{
                                $update_speen = SpennCompetitionTransactions::where('transaction_id', $request["RequestGuid"])->update(['status' => "Pending"]);
                                return response()->json([
                                    'message' => "Payment status not updated",
                                    'response_status' => 'failed'
                                ]);
                            }
                            break;
                        default:
                            return response()->json([
                                'message' => "Transactions Id doesn't exist",
                                'response_status' => 'failed'
                            ]);
                    }
                }

            }
        }
    }
    public function UpdateMomoPayment(Request $request){
        $external_payment_code = $request['external_payment_code'];
        $song_info = MomoTransaction::where('external_payment_code',$external_payment_code)->first();
        if($request['payment_status'] == "SUCCESSFUL"){
            $phonenumbers = $song_info->phone;
            $unlimitedamount = $song_info->amount;
            $song_name = $song_info->artist_name;
            $song_artist = $song_info->artist_song;
            $new_votess = $unlimitedamount / 50 ;

            $firstday = new Carbon('first day of last month');
            $lastday = new Carbon('last day of last month');
            $lastday_ = $lastday->subDays(7);
            $firstdaydate = $firstday->toDateString();
            $lastdaydate = $lastday_->toDateString();

            $dt = Carbon::now();
            $vote = new Votes();
            $vote->starting_date = $firstdaydate;
            $vote->ending_date = $lastdaydate;
            $vote->voterphonenumber = $phonenumbers;
            $vote->vote = $new_votess;
            $vote->voter_status = $request['payment_status'];
            $vote->voter_artist = $song_artist;
            $vote->voter_artist_song = $song_name;
            $vote->song_id = $song_info->song_id;
            $vote->save();
            $last_id = $vote->id;

            $votepivot = new SongVotes();
            $votepivot->song_id = $song_info->song_id;
            $votepivot->vote_id = $last_id;
            $votepivot->save();

            $Momopayment = MomoTransaction::where('external_payment_code',$external_payment_code)
                ->update([
                    'transactionid'=> $external_payment_code,
                    'voter_id'=> $last_id,
                    'status'=> $request['payment_status'],
                    'assignedid'=> $request['id'],
                    'company_name'=> $request['company_name'],
                    'code'=> $request['code'],
                    // 'amount'=> $responsedata[0]->amount,
                    'payment_code'=> $request['payment_code'],
                    'external_payment_code'=> $request['external_payment_code'],
                    'payment_status'=> $request['payment_status'],
                    'payment_type'=> $request['payment_type'],
                    'callback_url'=> $request['callback_url'],
                    'momodeleted_at'=> $request['deleted_at'],
                    'momocreated_at'=> $request['created_at'],
                    'momoupdated_at'=> $request['updated_at'],
                ]);
                return response()->json([
                    'message' =>"Payment status updated",
                    'response_status'=>$request['payment_status']
                ]);
        }else{
            $Momopayment = MomoTransaction::where('external_payment_code',$external_payment_code)
                ->update([
                    'transactionid'=> $external_payment_code,
                    'status'=> $request['payment_status'],
                    'assignedid'=> $request['id'],
                    'company_name'=> $request['company_name'],
                    'code'=> $request['code'],
                    // 'amount'=> $responsedata[0]->amount,
                    'payment_code'=> $request['payment_code'],
                    'external_payment_code'=> $request['external_payment_code'],
                    'payment_status'=> $request['payment_status'],
                    'payment_type'=> $request['payment_type'],
                    'callback_url'=> $request['callback_url'],
                    'momodeleted_at'=> $request['deleted_at'],
                    'momocreated_at'=> $request['created_at'],
                    'momoupdated_at'=> $request['updated_at'],
                ]);
                return response()->json([
                    'message' =>"Payment status not updated",
                    'response_status'=>$request['payment_status']
                ]);
        }
    }
    public function UpdateCompetitionMomoPayment(Request $request){
        $external_payment_code = $request['external_payment_code'];
        $contestant_info = CompetitionTransactions::where('external_payment_code',$external_payment_code)->first();

        if($request['payment_status'] == "SUCCESSFUL"){
            $phonenumbers = $contestant_info->phonenumber;
            $unlimitedamount = $contestant_info->amount;
            $contestant_name = $contestant_info->names;
            $competition_id_ = $contestant_info->competition_id;
            $contestant_id_ = $contestant_info->contestant_id;
            $tx_ref = $contestant_info->transactionid;
            $new_votess = $unlimitedamount / 50 ;

            $firstday = new Carbon('first day of last month');
            $lastday = new Carbon('last day of last month');
            $lastday_ = $lastday->subDays(7);
            $firstdaydate = $firstday->toDateString();
            $lastdaydate = $lastday_->toDateString();

            $dt = Carbon::now();
            $addvote = new CompVotes();
            $addvote->vote = $new_votess;
            $addvote->names = $contestant_name;
            $addvote->competition_id = $competition_id_;
            $addvote->contestant_id = $contestant_id_;
            $addvote->payment_from = "MOMO";
            $addvote->payment_tx_ref = $tx_ref;
            $addvote->status = "SUCCESSFUL";
            $addvote->save();
            $last_id = $addvote->id;

            $Momopayment = CompetitionTransactions::where('external_payment_code',$external_payment_code)
                ->update([
                'voter_id'=> $last_id,
                'vote'=> $new_votess,
                'transactionid'=> $external_payment_code,
                'status'=> $request['payment_status'],
                'assignedid'=> $request['id'],
                'company_name'=> $request['company_name'],
                'code'=> $request['code'],
                'payment_code'=> $request['payment_code'],
                'external_payment_code'=> $request['external_payment_code'],
                'payment_status'=> $request['payment_status'],
                'payment_type'=> $request['payment_type'],
                'callback_url'=> $request['callback_url'],
                'momodeleted_at'=> $request['deleted_at'],
                'momocreated_at'=> $request['created_at'],
                'momoupdated_at'=> $request['updated_at'],
            ]);
            return response()->json([
                'message' =>"Payment status updated",
                'response_status'=>$request['payment_status']
            ]);
        }else{
            $Momopayment = MomoTransaction::where('external_payment_code',$external_payment_code)
                ->update([
                    'transactionid'=> $external_payment_code,
                    'status'=> $request['payment_status'],
                    'assignedid'=> $request['id'],
                    'company_name'=> $request['company_name'],
                    'code'=> $request['code'],
                    // 'amount'=> $responsedata[0]->amount,
                    'payment_code'=> $request['payment_code'],
                    'external_payment_code'=> $request['external_payment_code'],
                    'payment_status'=> $request['payment_status'],
                    'payment_type'=> $request['payment_type'],
                    'callback_url'=> $request['callback_url'],
                    'momodeleted_at'=> $request['deleted_at'],
                    'momocreated_at'=> $request['created_at'],
                    'momoupdated_at'=> $request['updated_at'],
                ]);
            return response()->json([
                'message' =>"Payment status not updated",
                'response_status'=>$request['payment_status']
            ]);
        }
    }
    public  function  load_tiles(){
        $keyword = content_Titles::get();
        foreach($keyword as $key => $val) {
            $en[$val->keyword]=$val->content_title;
        }
        return response()->json([
            'Keywords' => $en,
            'response_status' =>true
        ]);

    }
    public function VoteNowCard(Request $request){

        $id = $request['id'];
        $song_info = SongInfo::where('id',$id)->first();

        $fname= $request['first_name'];
        $lname= $request['last_name'];
        $phone_number= $request['phone'];
        $customer_email= $request['email'];
        $city= $request['town_city'];
        $country= $request['country_id'];
        $pay_amount = $request['amount'];
        $fullname=$fname.' '.$lname;
        $song_name = $song_info->song_name;
        $song_artist = $song_info->song_artist;

        $new_votess = $pay_amount / 50;
        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $vote = new Votes();
        $vote->starting_date = $firstdaydate;
        $vote->ending_date = $lastdaydate;
        $vote->voterphonenumber = $phone_number;
        $vote->vote = $new_votess;
        $vote->voter_status = "SUCCESSFUL";
        $vote->voter_artist = $song_artist;
        $vote->voter_artist_song = $song_name;
        $vote->song_id = $id;
        $vote->save();
        $last_id = $vote->id;
        $hash= $request['txref'];
        $currency= "RWF";

        $votepivot = new SongVotes();
        $votepivot->song_id = $id;
        $votepivot->vote_id = $last_id;
        $votepivot->save();

        $Cardpayment = VotingCardTransactions::create([
            'first_name'=> $fname,
            'last_name'=> $lname,
            'phonenumber'=> $phone_number,
            'email'=> $customer_email,
            'city'=> $city,
            'country'=> $country,
            'amount'=> $pay_amount,
            'tx_ref'=> $request['txRef'],
            'transaction_id'=> $request['txRef'],
            'song_name'=> $song_name,
            'song_artist'=> $song_artist,
            'currency'=> $currency,
            'status'=> "successful",
            'payment_options'=> "Card",
            'voter_id'=> $last_id,
        ]);
        if($Cardpayment){
            return response()->json([
                'message' =>"Votes has been added, to vote again, fill the form and press Vote",
                'response_status'=> 200
            ]);
        }
    }
    public function VoteCompetitionNowCard(Request $request){

        $id = $request['id'];
        $contestant_info = Contestants::where('id',$id)->first();

        $fname= $request['first_name'];
        $lname= $request['last_name'];
        $phone_number= $request['phone'];
        $customer_email= $request['email'];
        $city= $request['town_city'];
        $country= $request['country_id'];
        $pay_amount = $request['amount'];
        $fullname=$fname.' '.$lname;
        $contestant_name = $contestant_info->names;
        $competition_id_ = $contestant_info->competition_id;
        $contestant_id_ = $contestant_info->id;

        $new_votess = $pay_amount / 50;
        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();


        $hash= $request['txref'];
        $currency= "RWF";
        $addvote = new CompVotes();
        $addvote->vote = $new_votess;
        $addvote->names = $contestant_name;
        $addvote->competition_id = $competition_id_;
        $addvote->contestant_id = $contestant_id_;
        $addvote->payment_from = "Card";
        $addvote->payment_tx_ref = $request['txref'];
        $addvote->status = "SUCCESSFUL";
        $addvote->save();
        $last_id = $addvote->id;

        $Cardpayment = CompetitionCardTransactions::create([
            'first_name'=> $fname,
            'last_name'=> $lname,
            'phonenumber'=> $phone_number,
            'email'=> $customer_email,
            'city'=> $city,
            'country'=> $country,
            'amount'=> $pay_amount,
            'tx_ref'=> $request['txRef'],
            'transaction_id'=> $request['txRef'],
            'competition_id'=> $competition_id_,
            'contestant_id'=> $contestant_id_,
            'currency'=> $currency,
            'status'=> "successful",
            'payment_options'=> "Card",
            'voter_id'=> $last_id,
        ]);

        if($Cardpayment){
            return response()->json([
                'message' =>"Votes has been added, to vote again, fill the form and press Vote",
                'response_status'=> 200
            ]);
        }

    }
}
