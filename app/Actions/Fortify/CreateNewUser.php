<?php

namespace App\Actions\Fortify;

use App\Models\AuthorDetails;
use App\Models\FreelancerDetails;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input){
        if($input['role'] == "Author"){
            Validator::make($input, [
                'first_name' => ['required'],
                'last_name' => ['required'],
                'profile_picture' => ['mimes:jpeg,bmp,png'],
                'role' => ['required'],
                'phone_number' => ['required'],
                'country_id' => ['required'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    Rule::unique(User::class),
                ],
                'password' => $this->passwordRules(),
            ])->validate();

            $image_profile = $input['profile_picture'];
            $image_name= rand() . '.' . $image_profile->getClientOriginalExtension();
            $image_profile->move(public_path('/UserProfile'), $image_name);
            $names = $input['first_name'].' '.$input['last_name'];
            $newUser = User::create([
                'name' => $names,
                'email' => $input['email'],
                'country_id' => $input['country_id'],
                'phone_number' => $input['phone_number'],
                'age' => $input['age'],
                'status' => "Active",
                'role' => $input['role'],
                'gender' => $input['gender'],
                'image_name' => $image_name,
                'password' => Hash::make($input['password']),
            ]);

            $add_details = new AuthorDetails();
            $add_details->user_id = $newUser->id;
            $add_details->author_city = null;
            $add_details->author_street_address = null;
            $add_details->author_company_name = null;
            $add_details->author_name = null;
            $add_details->author_website = null;
            $add_details->author_description = null;
            $add_details->author_facebook_url = null;
            $add_details->author_twitter_url = null;
            $add_details->author_instagram_url = null;
            $add_details->author_linkedin_url = null;
            $add_details->paypal_merchant_id = null;
            $add_details->save();

            return $newUser;

        }elseif ($input['role'] == "Freelancer"){
//            dd($input);
            Validator::make($input, [
                'first_name' => ['required'],
                'last_name' => ['required'],
                'profile_picture' => ['mimes:jpeg,bmp,png'],
                'role' => ['required'],
                'phone_number' => ['required'],
                'country_id' => ['required'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    Rule::unique(User::class),
                ],
                'password' => $this->passwordRules(),
            ])->validate();

            $image_profile = $input['profile_picture'];
            $image_name= rand() . '.' . $image_profile->getClientOriginalExtension();
            $image_profile->move(public_path('/UserProfile'), $image_name);
            $names = $input['first_name'].' '.$input['last_name'];

            $newUser = User::create([
                'name' => $names,
                'email' => $input['email'],
                'country_id' => $input['country_id'],
                'phone_number' => $input['phone_number'],
                'gender' => $input['gender'],
                'age' => $input['age'],
                'status' => "Active",
                'role' => "Freelancer",
                'image_name' => $image_name,
                'password' => Hash::make($input['password']),
            ]);

            $add_details = new FreelancerDetails();
            $add_details->user_id = $newUser->id;
            $add_details->freelancer_city = null;
            $add_details->freelancer_street_address = null;
            $add_details->freelancer_company_name = null;
            $add_details->freelancer_website = null;
            $add_details->freelancer_description = null;
            $add_details->freelancer_facebook_url = null;
            $add_details->freelancer_instagram_url = null;
            $add_details->freelancer_linkedin_url = null;
            $add_details->freelancer_twitter_url = null;
            $add_details->save();
            return $newUser;

        }else if($input['role'] == "Admin"){
//            dd($input);
            Validator::make($input, [
                'name' => ['required'],
                'profile_picture' => ['mimes:jpeg,bmp,png'],
                'role' => ['role'],
                'phone_number' => ['required'],
                'country_id' => ['required'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    Rule::unique(User::class),
                ],
                'password' => $this->passwordRules(),
            ])->validate();

            $image_profile = $input['profile_picture'];
            $image_name= rand() . '.' . $image_profile->getClientOriginalExtension();
            $image_profile->move(public_path('/UserProfile'), $image_name);

            return User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'gender' => $input['gender'],
                'country_id' => $input['country_id'],
                'phone_number' => $input['phone_number'],
                'age' => $input['age'],
                'status' => "Active",
                'role' => "Admin",
                'image_name' => $image_name,
                'password' => Hash::make($input['password']),
            ]);
        }
        else if($input['role'] == "reader"){
//            dd($input);
            Validator::make($input, [
                'first_name' => ['required'],
                'last_name' => ['required'],
                'profile_picture' => ['mimes:jpeg,bmp,png'],
                'role' => ['required'],
                'phone_number' => ['required'],
                'country_id' => ['required'],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    Rule::unique(User::class),
                ],
                'password' => $this->passwordRules(),
            ])->validate();

            $image_profile = $input['profile_picture'];
            $image_name= rand() . '.' . $image_profile->getClientOriginalExtension();
            $image_profile->move(public_path('/UserProfile'), $image_name);
            $names = $input['first_name'].' '.$input['last_name'];
            return User::create([
                'name' => $names,
                'email' => $input['email'],
                'gender' => $input['gender'],
                'country_id' => $input['country_id'],
                'phone_number' => $input['phone_number'],
                'age' => $input['age'],
                'status' => "Active",
                'role' => "reader",
                'image_name' => $image_name,
                'password' => Hash::make($input['password']),
            ]);
        }else{
//            dd($input);
            return Redirect::to("/");
        }
    }
}
