<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PalletRepairs extends Model
{
    use HasFactory;
    protected $table = "pallet_repairs";
    protected $fillable = ['pallet_management_id','repair_user_id','pallet_number_repairs'];
}
