<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PalletTransferTracking extends Model
{
    use HasFactory;
    protected $table = "pallet_transfer_trackings";
    protected $fillable = ['pallet_management_id','from_location','current_location','transfer_user_id'];
}
