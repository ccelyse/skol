<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pallet_distributor extends Model
{
    use HasFactory;
    protected $table = "pallet_distributors";
    protected $fillable = ['pallet_management_id','transfer_user_id','distributor_id'];
}
