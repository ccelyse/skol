<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory_Distributors extends Model
{
    use HasFactory;
    protected $table = "inventory_distributors";
    protected $fillable = ['distributor_name'];
}
