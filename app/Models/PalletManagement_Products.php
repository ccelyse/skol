<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PalletManagement_Products extends Model
{
    use HasFactory;
    protected $table = "pallet_management__products";
    protected $fillable = ['pallet_management_id','pallet_product_id'];
}
