<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransferAllList extends Model
{
    use HasFactory;
    protected $table = "transfer_all_lists";
    protected $fillable = ['pallet_management_id','transfer_user_id'];
}
