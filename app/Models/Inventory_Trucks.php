<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory_Trucks extends Model
{
    use HasFactory;
    protected $table = "inventory_trucks";
    protected $fillable = ['distributor_id','truck_number_plate'];
}
