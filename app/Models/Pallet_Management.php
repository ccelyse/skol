<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pallet_Management extends Model
{
    use HasFactory;
    protected $table = "pallet_management";
    protected $fillable = ['pallet_location_id','pallet_barcode','pallet_user_id'];
}
