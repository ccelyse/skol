<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory_Products extends Model
{
    use HasFactory;
    protected $table = "inventory_products";
    protected $fillable = ['product_name'];
}
